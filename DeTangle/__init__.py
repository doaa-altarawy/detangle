from flask import Flask
# from flask_mail import Mail
# from flask_sqlalchemy import SQLAlchemy
# from flask_login import LoginManager
from config import config
from celery import Celery
import os

# mail = Mail()
# db = SQLAlchemy()

# login_manager = LoginManager()
# login_manager.login_view = 'auth.login'

celery = Celery(__name__,
                broker=os.environ.get('CELERY_BROKER_URL', 'redis://localhost:6379/0'),
                backend=os.environ.get('CELERY_RESULT_BACKEND', 'redis://localhost:6379/0')
        )

# Import celery task so that it is registered with the Celery workers
# from .celery_tasks import long_task, short_task

def init_celery(app=None):

    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery

def create_app(config_name='default'):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    celery = init_celery(app)
    # celery.conf.update(app.config)

    # mail.init_app(app)
    # db.init_app(app)

    # login_manager.init_app(app)

    # if app.config['SSL_REDIRECT']:
    #     from flask_sslify import SSLify
    #     sslify = SSLify(app)

    # The main application entry
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    # For authentication
    # from .auth import auth as auth_blueprint
    # app.register_blueprint(auth_blueprint, url_prefix='/auth')

    # Remove the session after each request
    @app.teardown_appcontext
    def shutdown_session(exception=None):
        from .model.database import db_session
        db_session.remove()

    return app

