# -*- coding: utf-8 -*-
"""
Created on Mon Oct 31 22:12:37 2016

@author: doaa
"""

import numpy as np
import pandas as pd
from os.path import join


arab_dev_path = '/var/www/DeTangle/DeTangle/netAnalysis/data/Arabidopsis/grene'
expr_file = 'genes_WT.fpkm_tracking_AGI_Description.csv'

data = pd.read_csv(join(arab_dev_path, expr_file))
print(data.shape)
