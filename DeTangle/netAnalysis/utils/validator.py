import pandas as pd
import numpy as np


def validate_expr_tf_files(expr_file, tf_file):

    try:
        expr = pd.read_csv(expr_file, sep='\t')
        tf = pd.read_csv(tf_file, header=None)

        if tf.shape[1] != 1:
            return 'Transcription Factors file format error. It should be one column only.'

        missing = np.setdiff1d(tf[0].values, expr.columns.values)

        if len(missing) > 0:
            return 'The following Transcription Factors are in the TF file ' + \
                   'but are not present in the gene expression file! ' + \
                    str(missing)

        if expr.isnull().sum().sum() > 0:
            return 'Gene Expression file cannot have missing values.'
    except Exception as err:
        print(err)
        return 'Invalid Input files. Full error: \n' + str(err)

    return ''