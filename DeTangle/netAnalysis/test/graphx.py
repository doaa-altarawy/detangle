__author__ = 'doaa2012'

import networkx as nx
import matplotlib.pyplot as plt
from netAnalysis.utils import Datasets as ds
from netAnalysis.utils.InOut import InOutUtil as IO
import numpy as np


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def getTestGraph():
    g = nx.DiGraph()

    edges = []
    g.add_edge('G1', 3, weight=0.1)
    g.add_edge('G1', 4, weight=0.7)
    g.add_edges_from(
        [
            (1, 3), (1, 4), (5, 10), (5, 11), (10, 11)
        ])
    return g

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def runAnalysisTest(g):

    print("Nodes: ", g.nodes())
    print("Edges: ", g.edges(data = True))
    print("Edge Data: ", g.edges('G128'))  # out edges only!
    # print("Edge Data: ", g.edges('G175'))
    print("Neighbors: ", g.neighbors('G128')) # successors only

    ### Get connected components from node n, undirected only
    # undireG = g.to_undirected()
    # print("Connected undirected: ", [i for i in nx.connected_components(undireG)])
    # print("Connected undirected to G1: ", nx.node_connected_component(undireG, 'G175'))


    ### Get connected components

    # connectedNodes = nx.weakly_connected_components(g)
    # print("Weak Connected components: ", [i for i in connectedNodes])
    connectedSubgraphs = nx.weakly_connected_component_subgraphs(g)
    for i in connectedSubgraphs:
        print("- Info:\n======\n ", nx.info(i))
        # print(g.edges(data=True))

    ### Get neighbors of node n, then get all edges on them
    nodes = nx.all_neighbors(g ,'G128')     # all successors and predecessors
    ## equivalent to:
    #nodes = g.successors('G128')
    #nodes.extend(g.predecessors('G128'))

    # nodes.extend(g.successors(nodes))

    print(nodes)
    edges = g.edges(nodes, data=True)

    print("small Graph: ", edges)
    smallGraph = nx.DiGraph(edges)
    nx.draw_networkx(smallGraph)
    plt.savefig("graph.png")
    nx.write_adjlist(smallGraph, "test.adjlist")
    nx.write_weighted_edgelist(smallGraph, "net1_sub2.tsv", delimiter='\t')


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def getSubGraph(node, subGraphName):
    """
        Generate subgraph from a given hub node
        includes predecessors and sucessors of the given node
        and all the edges incident of them
    :param node:
    :return:
    """

    fileName = ds.datasets['Net1'].pathToData +\
                ds.datasets['Net1'].goldStd
    g = nx.read_weighted_edgelist(fileName, delimiter='\t',
                                   create_using=nx.DiGraph())
    print("Current Node: ", node)
    print("Out Degree: ", g.out_degree(node))
    print("In Degree: ", g.in_degree(node))

    ### Searching for nodes with low degree for testing
    # for n in g.nodes():
    #     if (g.degree(n)>5 and g.degree(n)<10):
    #         print("Degree of {} is {}".format(n, g.degree(n)))


    ### Get neighbors of node n, then get all edges on them
    # nodes = [i for i in nx.all_neighbors(g , node)]  # all successors and predecessors
    nodes = g.successors(node)          # successors
    nodes.extend(g.predecessors(node))  # predecessors
    nodes.append(node)                  # Add the hub node


    ### FOR TESTING
    # print("small Graph: \n", edges)
    smallGraph = g.subgraph(nodes)
    nx.draw_networkx(smallGraph)
    plt.savefig(ds.datasets[subGraphName].pathToData + "graph_"+subGraphName+".png")
    nx.write_adjlist(smallGraph, "test.adjlist")

    ### Save the new Dataset
    nx.write_weighted_edgelist(smallGraph,
               ds.datasets[subGraphName].pathToData +
                               ds.datasets[subGraphName].goldStd,
                               delimiter='\t')
    ### get expression data for the subset of nodes and save it
    saveSubsetExprData('Net1', subGraphName, nodes)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def saveSubsetExprData(bigDataName, subsetDataName, nodes):
    """
        get expression data for the subset of nodes and save it
    :param bigDataName:
    :param subsetDataName:
    :param nodes:
    :return:
    """
    # Get Expression data
    exprFile = ds.datasets[bigDataName].pathToData +\
               ds.datasets[bigDataName].exprFile

    tfFile = ds.datasets[bigDataName].pathToData +\
               ds.datasets[bigDataName].TF_File

    expMat = np.genfromtxt(open(exprFile,"rb"),
                            delimiter="\t", skip_header=0, dtype='|U20')
    tfNames = np.genfromtxt(open(tfFile,"rb"),
                            delimiter="\t", skip_header=0, dtype='|U20')

    geneNames = expMat[0, :]

    print("Len of nodes: " , len(nodes))
    indiceOfSubNodesInExpr = [0] * len(nodes)
    indiceOfSubNodesInTF = []
    for i, name in enumerate(nodes):
        indiceOfSubNodesInExpr[i] = np.where(geneNames==name)[0][0] # always returns 1 node
        indiceOfSubNodesInTF.extend(np.where(tfNames==name)[0]) # returns a node or empty

    print(len(indiceOfSubNodesInExpr))
    print(indiceOfSubNodesInExpr)
    print(len(indiceOfSubNodesInTF))
    print(indiceOfSubNodesInTF)
    subExprData = expMat[:, indiceOfSubNodesInExpr]
    subTFData = tfNames[indiceOfSubNodesInTF] # todo: intersect with indices to contents

    print(subExprData.shape)
     # Save the new expression file
    newExprFile = ds.datasets[subsetDataName].pathToData +\
                  ds.datasets[subsetDataName].exprFile
    np.savetxt(newExprFile, subExprData, delimiter='\t', fmt="%s")

    # Save the new TF file
    newTFFile = ds.datasets[subsetDataName].pathToData +\
                  ds.datasets[subsetDataName].TF_File
    np.savetxt(newTFFile, subTFData, delimiter='\t', fmt="%s")

# ===============================================================

def saveSubsetExprDataTest():
    """
        get expression data for the subset of nodes and save it
    :param bigDataName:
    :param subsetDataName:
    :param nodes:
    :return:
    """
    # Get Expression data
    expMat = np.array([['G1', '1'], ['G2', '2'], ['G3', '3'],
                       ['G4', '4'], ['G5', '5'], ['G6', '6']], dtype='|U20')

    nodes = ['G2', 'G5', 'G4']

    geneNames = expMat[:, 0]

    tfNames = np.array(['G1', 'G3', 'G4', 'G5', 'G6'])

    print("Len of nodes: " , len(nodes))
    indiceOfSubNodesInExpr = [0] * len(nodes)
    indiceOfSubNodesInTF = []
    for i, name in enumerate(nodes):
        indiceOfSubNodesInExpr[i] = np.where(geneNames==name)[0][0] # always returns 1 node
        indiceOfSubNodesInTF.extend(np.where(tfNames==name)[0]) # returns a node or empty

    print(len(indiceOfSubNodesInExpr))
    print(indiceOfSubNodesInExpr)
    print(len(indiceOfSubNodesInTF))
    print(indiceOfSubNodesInTF)
    subExprData = expMat[indiceOfSubNodesInExpr, :]
    subTFData = tfNames[indiceOfSubNodesInTF] # todo: intersect with indices to contents

    print(subExprData)
    print(subTFData)



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def testSearchGetIndex():
    a = np.array(['b', 'b', 'c', 'a', 'd', 'x'])
    print(a)
    query = np.array(['a', 'x'])

    ### Solution 1
    indx = a.argsort()      # indices that would sort the array
    i = np.searchsorted(a[indx], query)  # indices in the sorted array
    result = indx[i]        # indices with respect to the original array

    print(result)
    print(a[result])


    #### OR Solution 2

    result = []
    for i in query:
        indx = np.where(a==i)[0]
        result.extend(indx)

    print(result)
    print(a[result])

    ###### OR >>>>>>>>>>>>>>>>>>>>>>> This one
    print(query[:,None])    # query transpose
    result = np.where(a==query[:,None])[1]  # [0]: indices in query
                                            # [1]: corresp indices in a
    print(result)
    print(a[result])

    ### OR
    result = np.fromiter(((a==i).nonzero()[0] for i in query), dtype=int)
    print(result)
    print(a[result])


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if __name__ == "__main__":
    # testSearchGetIndex()
    # getSubGraph('G28', 'Subgraph1')
    # getSubGraph('G192', 'Subgraph2')
    # getSubGraph('G5', 'Subgraph3')
    # getSubGraph('G126', 'Subgraph4')

    # Save subset of the data that has known interactions
    # dataset = ds.datasets['Net3']
    # goldStdGraph = IO.readGraph(dataset.pathToData + dataset.goldStd,
    #                             skipHeader=False)
    # saveSubsetExprData('Net3', 'Net3_small', goldStdGraph.getNodes())

    saveSubsetExprDataTest()