#! /var/www/DeTangle/DeTangle/venv python3

# Activate virtual env for testing the file
# activate_this = '/var/www/DeTangle/DeTangle/venv/bin/activate_this.py'
# execfile(activate_this, dict(__file__=activate_this))
# exec(open(activate_this).read(), dict(__file__=activate_this))


# import numpy as np
import csv
from subprocess import call
import time
from .Inferelator_Refactor import Inferelator_rpy

from .utils.Datasets import Dataset


class Inferelator:
    """
    @summary
    @param
    """
    pathToScripts = r'./Inferelator_Refactor/scripts/'
    pathToData = r'./DREAM5/'
    command = r'Rscript ' + pathToScripts + 'runInf.R '
    args =  r'--inf_max_reg 20 ' \
            r'--n_boots 1 ' \
            r'--tau 15 ' \
            r'--num_pred 10000 ' \
            r'--num_processors 2 ' \
            r'--path_to_scripts ' + pathToScripts

    datasets = dict()
    datasets['net1'] = Dataset(pathToData+'Network1/',
                        'net1_expression_data.tsv',
                        'net1_transcription_factors.tsv',
                        'net1_chip_features.tsv')

    datasets['net1_small'] = Dataset(pathToData+'Network1/',
                            'net1_expression_data_small.tsv',
                            'net1_transcription_factors.tsv',
                            'net1_chip_features.tsv')

    datasets['net1_small_noChip'] = Dataset(pathToData+'Network1/',
                            'net1_expression_data_small.tsv',
                            'net1_transcription_factors.tsv')

    datasets['net2'] = Dataset(pathToData+'Network2/',
                        'net2_expression_data.tsv',
                        'net2_transcription_factors.tsv',
                        'net2_chip_features.tsv')



    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    data_net1_small = r'--data_file ' + pathToData + \
                      'Network1_small/net1_expression_data_small.tsv ' \
                      r'--reg_file ' + pathToData + \
                      'Network1_small/net1_transcription_factors.tsv ' \
                      r'--meta_file ' + pathToData + \
                      'Network1_small/net1_chip_features.tsv '

    data_net1 = r'--data_file ' + pathToData + \
                'Network1/net1_expression_data.tsv ' \
                r'--reg_file ' + pathToData + \
                'Network1/net1_transcription_factors.tsv ' \
                r'--meta_file ' + pathToData + \
                'Network1/net1_chip_features.tsv '

    data_net1_small_noTF = r'--data_file ' + pathToData + \
                     'Network1_small/net1_expression_data_small.tsv ' \
                     r'--reg_file ' + pathToData + \
                     'Network1_small/net1_transcription_factors.tsv'
                    # r'--meta_file ' + pathToData +
                    # 'Network1_small/net1_chip_features_small.tsv '

    sample_small = data_net1_small_noTF

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



    @staticmethod
    def readTSV(exprFile, max_lines=0, isChip=False):
        with open(Inferelator.pathToData+exprFile, 'rU') as infile:
            # with open(Inferelator.pathToData+exprFile.split('.')[0]+'_small.tsv', 'w') as outfile: # same as:
            with open(Inferelator.pathToData+exprFile[:-4]+'_small.tsv', 'w') as outfile:
                reader = csv.reader(infile, delimiter="\t")
                writer = csv.writer(outfile, delimiter='\t')
                # Read header
                header = next(reader)
                numOfCol = len(header)
                writer.writerow(header)
                for line in reader:     # data rows
                    if (len(line)==0):  # skip empty lines
                        continue
                    if (isChip and int(line[0])<max_lines) or \
                            (not isChip and len(line)==numOfCol):  # max num of lines in expr files, or skip expr# >max in chip file
                        writer.writerow(line)
                    else:
                        print('line num {} is skipped'.format(reader.line_num))
                    # If max number of rows reached, break
                    if(not isChip and max_lines!=0 and reader.line_num==max_lines):
                        break

    @staticmethod
    def runInferelator(data):
        """
        Calls R code of Inferelator
        :return:
        """
        start = time.time()
        call(Inferelator.command + data + Inferelator.args, shell=True)
        # Inferelator_rpy.run_Inf_R(data + Inferelator.args)
        runTime = (time.time() - start) / 60.0
        print('Running Time = {:.2f} mins'.format(runTime))

    @staticmethod
    def convertToSize(size):
        """
        Copy small part of the large input data file
        :return:
        """
        exprFile = 'Network1_small/net1_expression_data.tsv'
        chipFile = 'Network1_small/net1_chip_features.tsv'
        Inferelator.readTSV(exprFile, size)
        Inferelator.readTSV(chipFile, size, True)

    # def __del__(self):
    #     '''ends R instance before exit --> not working # TODO
    #     '''
    #     Inferelator_rpy.end_R()

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def mainTest():
    # Inferelator.convertToSize(20)
    print("Data file:{}\n\n".format(Inferelator.sample_small))
    Inferelator.runInferelator(Inferelator.sample_small)

if __name__ == "__main__":
    mainTest()

