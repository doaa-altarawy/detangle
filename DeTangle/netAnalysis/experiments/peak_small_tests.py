from __future__ import absolute_import
__author__ = 'doaa'

import csv, datetime
from netAnalysis.experiments.expr_util import run_Peak_test, evaluatePrediction
from netAnalysis.utils import Datasets as ds
from collections import OrderedDict
from netAnalysis.scoring.PR_ROC import get_accuracy_realData
import pandas as pd


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if __name__ == "__main__":

    datasetName = 'Root' #, 'Net4', 'Grene' 'Net3_conn_final'
    # datasetName = 'Net4'
    # datasetName = 'Net1'


    alphas_small = [0.001, 0.01, 0.05, 0.1, 0.5, 1]
    alphas_large = [0.1, 0.2, 0.4, 0.6, 0.8, 1, 2, 3, 5]
    #
    params = OrderedDict([('fileName', ''), ('priorPercent', 1), ('falsePriorRatio', 0),
                          ('priorWeight', 0.01), ('priorFile', None), ('pkEachGene', 1),
                          ('alpha', 0.1), ('l1_ratio', 0.5), ('isCV', 1), ('fit_intercept', 1),
                          ('scaleX', 1), ('halfLife', 10), ('method', 'PenaltyScaling'), ('freeCV', 1),
                          ('negativePriorPercent', 0)])

    # params = OrderedDict([('fileName', ''), ('priorPercent', 100), ('falsePriorRatio', 0),
    #                       ('priorWeight', 0.01),
    #                       ('alpha', 0.1), ('l1_ratio', 0.5), ('isCV', 1), ('fit_intercept', 1),
    #                       ('scaleX', 1), ('halfLife', 10), ('method', 'PenaltyScaling'), ('freeCV', 1)])


    filename,  pk_log = run_Peak_test(datasetName, params, alphas_large)
    pathToPred = ds.datasets[datasetName].get_outFile() + filename
    # pathToPred = '/var/www/DeTangle/DeTangle/data/DREAM5/Network4/output/NetworkInference_Network4_priorPerct100_weight0.01__scaled__Mar-09_13-01--45PenaltyScaling::InfOnly'
    # pk_log = pd.read_csv('PK_log.csv')

    print('Path to pred: ', pathToPred)
    evaluatePrediction(pk_log, pathToPred)
    print(pk_log)
    # AUROC, AUPR, _, _ = get_accuracy_realData(network=datasetName, predFile=pathToPred, visualize=False)

    pk_log.to_csv('PK_log_full4.csv', index=False)

    # pathToCombined = pathToPred[:-9] + "::Combined"
    # AUROC, AUPR, _, _ = get_accuracy_realData(network=datasetName, predFile=pathToCombined, visualize=True)

    # mapGeneNames(datasetName=datasetName, filename=pathToPred)


    # print(paramsFileName)
    # filename = ds.datasets['Root'].get_outFile() + '_priorPerct100_weight0.01__scaled__Nov-18_18-22--03FeatureScaling::InfOnly'
    # mapGeneNames(datasetName='Root', filename=filename)

