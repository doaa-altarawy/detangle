__author__ = 'doaa'

from ..Peak import *
import numpy as np
import csv, datetime
from ..utils import Datasets as ds
from ..utils import helpers as hlp
from ..utils import Datasets as ds
from ..utils.config import *
from os.path import join


def run_Peak_test(datasetName, params, alphas=None, runCLR=False):
    """Main test for Peak py using the given dataset"""

    pk_log = pd.DataFrame(columns=['GeneName', 'TFsList', 'NumTF', 'NumTrueP', 'NumFalseP'])

    params['fileName'] = "_priorPerct"+str(params['priorPercent'])+\
                        "_weight"+str(params['priorWeight'])+"_"
    if (params['scaleX']):
        params['fileName'] += '_scaled_'

    params['fileName'] += '_{0:%b}-{0:%d}_{0:%H}-{0:%M}--{0:%S}'\
                                .format(datetime.datetime.now())
    params['fileName'] += params['method']

    print('----- Dataset: {}----'.format(datasetName))
    print("Running Inference for params:", params)

    dataset = ds.datasets[datasetName]
    inf = Peak(dataset, maxPredictors=30, # use maxPredictors=0 to use all genes
                      halfLife=params['halfLife'],
                      scaleX=params['scaleX'],
                      alpha=params['alpha'],
                      l1_ratio=params['l1_ratio'],
                      useCV=params['isCV'],
                      alphas=alphas,
                      verbose=False,
                      maxItr=500,
                      fit_intercept=params['fit_intercept'])

    clr_data_path = get_pathToCLRData()
    if runCLR:
        # or datasetName not in ['Net1', 'Net3_conn_final', 'Net4', 'Grene_all', 'Grene']:
        # Run R code to calculate CLR
        inf.getMixedCLR(clrOnly=True, outPath=clr_data_path)

    # Read CLR saved output
    inf.readInputData(join(clr_data_path, str(datasetName)+ "_mixedCLRMatrixAll.csv"),
                      join(clr_data_path, str(datasetName)+"_X_lars.csv"),
                      join(clr_data_path, str(datasetName)+ "_Y_lars.csv"))

    priorFile = params['priorFile'] if 'priorFile' in params else None
    pkEachGene = params['pkEachGene'] if 'pkEachGene' in params else None
    priorTFs = getTrueFalsePrior(dataset, inf.geneNames, inf.tfNames, params['priorPercent'],
                                    params['falsePriorRatio'], priorFile, pkEachGene, pk_log)

    negativePrior = getNegativePrior(dataset, inf.geneNames, inf.tfNames,
                                     params['negativePriorPercent'], priorFile, pkEachGene)

    inf.predict_GRN(priorTFs=priorTFs, priorWeight=params['priorWeight'],
                    method=params['method'], negative_TFs=negativePrior)

    # Get combined predictions
    resultCLR, resultInf, resultComb = inf.getConbinedInfCLR_Scores(scaleResults=True)   # Inf + CLR

    #print("Results:", resultInf)
    if (params['isCV']==1):
        params['alpha'] = inf.cv_alpha

    print('Alpha---------', inf.cv_alpha)

    if (params['method'] == 'PenaltyScaling' and params['priorPercent']==0):
        inf.savePredictions(resultCLR, dataset.get_outFile()+params['fileName']+"::CLROnly")

    inf.savePredictions(resultInf, dataset.get_outFile()+params['fileName'] + "::InfOnly")
    inf.savePredictions(resultComb, dataset.get_outFile()+params['fileName'] + "::Combined")

    return params['fileName'] + "::InfOnly", pk_log

    # # Map gene names to real names and save
    # # print('ResultInf', resultInf)
    # resultMap = pd.DataFrame(resultInf, columns=['From', 'To', 'Weight'])
    # # print ('results: ', resultMap)
    # resultMap.From = map(lambda x: inf.geneNames[x], resultMap.From)
    # resultMap.To = map(lambda x: inf.geneNames[x], resultMap.To)
    #
    # # print('old gene names: ', resultMap)
    # resultMap.From = hlp.mapGeneNames(resultMap.From, dataset)
    # resultMap.To = hlp.mapGeneNames(resultMap.To, dataset)
    #
    # print('After mapping: ', resultMap)
    #
    # resultMap.to_csv(dataset.pathToData + dataset.outFile() +
    #                     params['fileName']+"::InfOnly_realNames",
    #                     sep='\t', index=False)

# ----------------------------------------------------------------------------

def mapGeneNames(datasetName='', filename=''):

    dataset = ds.datasets[datasetName]
    network = pd.read_csv(filename, sep='\t', header=None)
    network[0] = hlp.mapGeneNames(network[0], dataset)
    network[1] = hlp.mapGeneNames(network[1], dataset)
    print (network)
    network.to_csv(filename + '_realNames', index=False, sep='\t', header=False)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def readPriorKnowledge(dataset, geneNames, priorPercent, priorFile, pkEachGene=1, pk_log=None):
    """ Read prior knowledge from gold standard file
    and return percent of the perior equals to priorPercent

    :param dataset: dataset name, of type Dataset
    :param geneNames: list of gene names in the dataset
    :param priorPercent: percent of prior to return
    :param priorFile: if not None, read PK from that file
    :param pkEachGene : read PK percent for each target gene

    :returns priorTFs: adjacency list of genes with its TFs
            ex. (with TF3 is replaced with its index in geneNames
                G1: [TF3, TF4, TF8]
                G2: []
                G3: [TF4, TF5]
    """

    if (np.isclose(priorPercent, 0)):
        return None

    # read gold std (prior knowledge) graph
    if priorFile is None:
        filename = dataset.get_goldStd()
    else:
        filename = priorFile

    if (os.path.getsize(filename) > 0):
        goldStd = pd.read_csv(filename, sep='\t', header=None)
    else:
        return None

    numOfPriorsToUse = int(goldStd.shape[0] * priorPercent / 100.0)
    priorTFs = {} # np.zeros(len(geneNames), dtype=object)  # Prior list for every gene
    geneNamesDF = pd.DataFrame(geneNames)

    if pkEachGene:
        # For all genes, get its TFs in priorTFs[gene] as a list
        for i, node in enumerate(geneNames):
            trueTFNames = goldStd[goldStd[1] == node][0].values
            trueTFIndices = geneNamesDF[geneNamesDF.isin(trueTFNames).values].index.values
            priorTFs[i] = trueTFIndices  # save TF index not its name

        # choose only priorPercent for each gene
        for geneIndex in priorTFs: # for each key (geneIndex)
            geneList = priorTFs[geneIndex]
            numOfPriorsToUse = int(len(geneList) * priorPercent / 100.0)
            if pk_log is not None:
                pk_log.loc[geneIndex] = geneNames[geneIndex], geneNames[priorTFs[geneIndex]], \
                                        len(geneList), 0, 0
            priorTFs[geneIndex] = priorTFs[geneIndex][:numOfPriorsToUse]

    else:

        goldStd = goldStd.iloc[:numOfPriorsToUse]

        # For all genes, get its TFs in priorTFs[gene] as a list
        for i, node in enumerate(geneNames):
            trueTFNames = goldStd[goldStd[1]== node][0].values
            trueTFIndices = geneNamesDF[geneNamesDF.isin(trueTFNames).values].index.values
            priorTFs[i] = trueTFIndices   # save TF index not its name

        print("numOfPriorsToUse Prior knowledge:", numOfPriorsToUse)

    print('Prior TF list:', priorTFs)

    return priorTFs

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def getTrueFalsePrior(dataset, geneNames, tfNames, truePriorPercent, falsePriorRatio,
                            priorFile=None, pkEachGene=False, pk_log=None):
    """ Read prior knowledge from gold standard file
    and retuen true prior with percentage = truePriorPercent
    and false prior with ratio = falsePriorRatio

    :param dataset: dataset name, of type Dataset
    :param geneNames: list of gene names in the dataset
    :param truePriorPercent: percent of true prior to use
    :param falsePriorRatio: the ration of false prior, i.e.:
                true:false  =  1:falsePriorRatio
    :param priorFile: if not None, load prior knowledge from that file
    :param pkEachGene : read PK percent for each target gene

    :returns priorTFs: adjacency list of genes with its TFs
    """

    if (np.isclose(truePriorPercent, 0)):
        return None;

    # Read True prior interactions
    priorTFs = readPriorKnowledge(dataset, geneNames, truePriorPercent, priorFile, pkEachGene, pk_log)
    allPriorTFs = readPriorKnowledge(dataset, geneNames, 100., priorFile, pkEachGene)

    numOfFalsePrior = int(len(allPriorTFs) * truePriorPercent / 100.0) * falsePriorRatio
    print('Number of negative prior======', numOfFalsePrior)

    currentCount = 0

    # falsePrior = np.zeros(len(geneNames), dtype=object)  # Prior list for every gene
    # For all genes, get its TFs in priorTFs[gene] as a list
    while (currentCount < numOfFalsePrior):
        gene_idx = np.random.random_integers(0, len(geneNames)-1) # random gene index
        TF_name = np.random.choice(tfNames) # random TF name
        TF_idx = np.where(geneNames == TF_name)[0][0] # find index of that TF gene

        if (TF_idx in allPriorTFs[gene_idx]):
            print(gene_idx, TF_idx, 'already found as True interaction.')
            continue
        # Add false prior
        print("adding gene: {}, TF: {}".format(gene_idx, TF_idx))
        priorTFs[gene_idx] = np.append(priorTFs[gene_idx], TF_idx)
        currentCount += 1

    print("numOfFalsePrior Prior knowledge:", numOfFalsePrior)

    # merge true and false prior
    return priorTFs

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def getNegativePrior(dataset, geneNames, tfNames, priorPercent, priorFile=None, pkEachGene=False):
    """ Generate negative prior knowledge that does not exist in the True list

    :param dataset: dataset name, of type Dataset
    :param geneNames: list of gene names in the dataset
    :param priorPercent: percent of negative prior to use
    :param priorFile: if not None, load prior knowledge from that file
    :param pkEachGene : read PK percent for each target gene

    :returns neagtivePriorTFs: adjacency list of genes with its nagative TFs
    """

    if (np.isclose(priorPercent, 0)):
        return None;

    # Read True prior interactions
    negativePriorTFs = dict([(i, []) for i in range(len(geneNames))])
    allPriorTFs = readPriorKnowledge(dataset, geneNames, 100., priorFile, pkEachGene)

    numOfNegPrior = int(len(allPriorTFs) * priorPercent / 100.0)
    print('Number of negative prior======', numOfNegPrior)

    currentCount = 0

    # Generate negative prior knowledge that does not exist in the True list
    while (currentCount < numOfNegPrior):
        gene_idx = np.random.random_integers(0, len(geneNames)-1) # random gene index
        TF_name = np.random.choice(tfNames)           # random TF name
        TF_idx = np.where(geneNames == TF_name)[0][0] # find index of that TF gene

        # if found, try another random TF
        if (TF_idx in allPriorTFs[gene_idx]):
            print(gene_idx, TF_idx, 'already found as True interaction.')
            continue

        # Add the negative prior
        print("adding gene: {}, TF: {}".format(gene_idx, TF_idx))
        negativePriorTFs[gene_idx].append(TF_idx)

        currentCount += 1


    print('Negative Prior TFs', negativePriorTFs)

    return negativePriorTFs

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def evaluatePrediction(pk_log, prediction_file):
    """ Find True positive and false positive
    """

    predictions = pd.read_csv(prediction_file, sep='\t', header=None)
    predictions = predictions[:10000]

    pk_log['Predicted'] = 0
    for i, gene in enumerate(pk_log['GeneName']):
        predicted_TF = predictions[predictions[1] == gene][0].values
        true_TF = pk_log['TFsList'].iloc[i]
        print(np.intersect1d(predicted_TF, true_TF))
        pk_log['NumTrueP'].iloc[i] = len(np.intersect1d(predicted_TF, true_TF))
        pk_log['NumFalseP'].iloc[i] = len(np.setdiff1d(predicted_TF, true_TF))
        pk_log['Predicted'].iloc[i] = predicted_TF
