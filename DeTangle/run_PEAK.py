#!/usr/bin/env python
from __future__ import division, print_function

from os import sys, path, environ
import site

# Enable virtual env
env_path = environ.get('ENV_PATH')
site.addsitedir(env_path)

# work around to be able to run this script as a module
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import DeTangle
__package__ = 'DeTangle'


import pandas as pd
import numpy as np
from .netAnalysis.Peak import Peak
from datetime import datetime
from .netAnalysis.utils import Datasets as ds
from .netAnalysis.experiments.expr_util import readPriorKnowledge, getNegativePrior
from .model import database as db
import argparse
import logging
from os.path import join
import pandas as pd


log = logging.getLogger(__name__)

# logging.basicConfig(
#                     # disable_existing_loggers=False,
#                     filename=join('log', 'run_peak.log'),
#                     level=logging.DEBUG,
#                     format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s")


def run(network_prefix, data_path, runCLR=True, half_life=10, method='PenaltyScaling'):
    """Run PEAK using the given network data"""

    dataset = ds.Dataset(pathToData=data_path,
                         dsName=network_prefix,
                         exprFile=network_prefix+'_expr.csv',
                         TF_File=network_prefix+'_tf.csv',
                         chipFile=network_prefix+'_chip.csv',
                         goldStd=network_prefix+'_pk.csv',
                         outFile=network_prefix+'_out.csv'
                         )

    log.info('----- Dataset: {}----'.format(dataset.dsName))
    log.info("Running Inference on {} ".format(datetime.now()))

    inf = Peak(dataset, maxPredictors=30, # use maxPredictors=0 to use all genes
                      halfLife=half_life if half_life else 10,
                      scaleX=1,
                      alpha=None,
                      l1_ratio=0.5,
                      useCV=1,
                      alphas=None,
                      verbose=False,
                      maxItr=200,
                      fit_intercept=1)

    clr_data_path = data_path #get_pathToCLRData()
    # Run R code to calculate CLR, then read output
    if runCLR:
        inf.getMixedCLR(clrOnly=True, outPath=clr_data_path)
    # read saved data
    log.info("Path to CLR data: {}".format(clr_data_path))
    inf.readInputData(join(clr_data_path, dataset.dsName + "_mixedCLRMatrixAll.csv"),
                      join(clr_data_path + dataset.dsName + "_X_lars.csv"),
                      join(clr_data_path + dataset.dsName + "_Y_lars.csv"))

    priorFile = dataset.get_goldStd()
    # log.info('path to gold std: {}'.format(priorFile))
    priorTFs = readPriorKnowledge(dataset=dataset, geneNames=inf.geneNames,
                                  priorPercent=100, priorFile=priorFile)

    inf.predict_GRN(priorTFs=priorTFs, priorWeight=0.01, method=method)

    log.info("PEAK Prediction done at: {}".format(datetime.now()))
    # Get combined predictions
    resultCLR, resultInf, resultComb = inf.getConbinedInfCLR_Scores(scaleResults=True)   # Inf + CLR

    log.debug("Results: {}".format(resultInf))
    log.debug('Alpha---------{}'.format(inf.cv_alpha))

    inf.savePredictions(resultCLR, dataset.get_outFile() + "::CLROnly")

    inf.savePredictions(resultInf, dataset.get_outFile())# + "::InfOnly")
    inf.savePredictions(resultComb, dataset.get_outFile() + "::Combined")

    return dataset.get_outFile()

# -------------------------------------------------------------

def run_online_peak(job_id, network_prefix, data_path, added_edges, deleted_edges):
    """Run online PEAK for the updated nodes only"""

    if len(added_edges)==0 and len(deleted_edges)==0:
        log.info('No change to run')

    print('Job ID: {}'.format(job_id))
    print('Network prefix: {}'.format(network_prefix))
    print('Data path: {}'.format(data_path))
    print('Added edges:\n {}'.format(added_edges))
    print('Deleted edges:\n {}'.format(deleted_edges))
    print('---------------------------------------------')

    # affected genes:
    targets_names = set()
    if len(added_edges) > 0:
        targets_names = targets_names.union(added_edges.target)

    if len(deleted_edges) > 0:
        targets_names = targets_names.union(deleted_edges.target)

    print('Affected genes: {}'.format(targets_names))

    # Run PEAK for the targets only with their PK


    dataset = ds.Dataset(pathToData=data_path,
                             dsName=network_prefix,
                             exprFile=network_prefix + '_expr.csv',
                             TF_File=network_prefix + '_tf.csv',
                             chipFile=network_prefix + '_chip.csv',
                             goldStd=network_prefix + '_pk.csv',
                             outFile=network_prefix + '_out.csv'
                             )

    print('----- Dataset: {}----'.format(dataset.dsName))
    print("Running Inference on {} ".format(datetime.now()))

    inf = Peak(dataset, maxPredictors=30,  # use maxPredictors=0 to use all genes
               halfLife=10, #half_life,
               scaleX=1,
               alpha=None,
               l1_ratio=0.5,
               useCV=1,
               alphas=None,
               verbose=False,
               maxItr=200,
               fit_intercept=1)

    clr_data_path = data_path  # get_pathToCLRData()
    # read saved data
    log.info("Path to CLR data: {}".format(clr_data_path))
    inf.readInputData(join(clr_data_path, dataset.dsName + "_mixedCLRMatrixAll.csv"),
                      join(clr_data_path + dataset.dsName + "_X_lars.csv"),
                      join(clr_data_path + dataset.dsName + "_Y_lars.csv"))

    priorFile = dataset.get_goldStd()
    # log.info('path to gold std: {}'.format(priorFile))
    priorTFs = readPriorKnowledge(dataset=dataset, geneNames=inf.geneNames,
                                  priorPercent=100, priorFile=priorFile)

    print('Gene names: ', inf.geneNames)

    print('PriorTFs before: ----', priorTFs)
    # Get indices of target genes
    targets_indices = []
    for gene in targets_names:
        # print('In gene --------------', gene)
        if not priorTFs:
            priorTFs = {}

        curr_gene = np.argwhere(inf.geneNames == gene)[0][0]
        targets_indices.append(curr_gene)
        tfs = pd.DataFrame()
        if len(added_edges)>0:
            tfs = tfs.append(added_edges[added_edges['target']==gene])
        if len(deleted_edges)>0:
            tfs = tfs.append(deleted_edges[deleted_edges['target'] == gene]) ####--------------<<<
        print('Tfs: ', tfs)

        tf_indices = [np.argwhere(inf.geneNames==tf)[0][0] for tf in tfs['source']]
        if (curr_gene not in priorTFs):
            priorTFs[curr_gene] = []

        priorTFs[curr_gene].extend(tf_indices)


    print('Target_indices: ', targets_indices)
    print('Prior TFs after: \n', priorTFs)

    inf.predict_GRN(priorTFs=priorTFs, priorWeight=0.01, method='PenaltyScaling',
                        target_genes=targets_indices)

    # print
    for gene in inf.predict_coef_:
        print('predict_coef {}: \n{}'.format(gene, inf.predict_coef_[gene]))

    # save/return predictions
    _, resultInf, _ = inf.getConbinedInfCLR_Scores(scaleResults=False)  # Inf + CLR

    print("Results: {}".format(resultInf))
    # print('Alpha---------{}'.format(inf.cv_alpha))

    inf.savePredictions(resultInf, dataset.get_outFile()+'_online')  # + "::InfOnly")

    return dataset.get_outFile()+'_online'


# -------------------------------------------------------------

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='PEAK: Gene regulatory network inference')
    parser.add_argument('--job_id', help='Job ID', required=True)
    parser.add_argument('--network_prefix', help='Prefix of the data', required=True)
    parser.add_argument('--data_path', help='Path to the data', required=True)
    args = vars(parser.parse_args())
    log.info('------------- New Call to PEAK ----------------')
    log.info("Calling PEAK with args {}".format(args))

    status = -1
    try:
        run(args['network_prefix'], args['data_path'])
        # Mark job as successfully done in the database
        status = 1
        msg = 'success'
    except OSError as e:
        msg = 'Input files format are invalid. Please check sample input.'
        log.error(e, exc_info=True)
    except Exception as e:
        log.error(e, exc_info=True)
        msg = 'An error occured while processing your data.'
    finally:
        # Save status in the DB
        db.set_job_end(args['job_id'], status=status)

        # send email that job is ended with msg
        # TODO
        log.info(msg)

    log.info('-------------- PEAK done (status = {}) --------------'.format(status))

