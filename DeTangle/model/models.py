import sqlalchemy as db
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, deferred
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class Network(Base):
    __tablename__ = 'networks'

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    dataset_name = db.Column(db.String(150), nullable=True)
    email = db.Column(db.String(150), unique=False)
    job_start = db.Column(db.DateTime, nullable=False)
    job_end = db.Column(db.DateTime, nullable=True)
    data_prefix = db.Column(db.String(50), unique=False, nullable=False)
    status = db.Column(db.Integer, nullable=False, default=0) # 0: running, 1:done, -1: error
    half_life = db.Column(db.Integer, nullable=True)

    # expression = db.deferred(db.Column(db.BLOB))     # deferred loading, large data
    # trans_factor = db.deferred(db.Column(db.BLOB))
    # prior_knowledge = db.deferred(db.Column(db.BLOB))
    # clr = db.deferred(db.Column(db.BLOB))
    #
    # cyo_graph = deferred(db.Column(db.BLOB))  # TODO: make it one-to-many

    # expr_id = db.Column(db.Integer, db.ForeignKey('expression.id'))
    # expression = db.relationship('Expression')


    def __repr__(self):
        return '<Network: Job ID: %s, data_prefix: %s, dataset_name: %s, email: %s, status" %s>' % \
               (self.id, self.data_prefix, self.dataset_name, self.email, self.status)

    def serialize(self):
        return {
            "job_id": self.id,
            "dataset_name": self.dataset_name,
            "email": self.email,
            "job_start": self.job_start,
            "job_end": self.job_end,
            "data_prefix": self.data_prefix
        }

# --------------------------------------------------------------

# class Expression(Base):
#     __tablename__ = 'expression'
#
#     id = Column(db.Integer, primary_key=True)
#     data = Column(String)

