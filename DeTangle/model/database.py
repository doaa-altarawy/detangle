from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from .models import Network, Base
from datetime import datetime
import os
# from flask import current_app

# no application context
# print('In Database:::', current_app._get_current_object().config)

uri = os.environ.get('DEV_DATABASE_URL') or \
      os.environ.get('TEST_DATABASE_URL') or \
      os.environ.get('DATABASE_URL')

engine = create_engine(uri, convert_unicode=True)

# Session scope must be used if flask-sqlalchemy is not handling the session
# db_session.query(Network) is equivalent to db_session().query(Network)
db_session = scoped_session(sessionmaker(autocommit=False, bind=engine))
# Base.query = db_session.query_property()


def init_db():
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    # import models
    Base.metadata.create_all(bind=engine)

init_db()

def drop_all():
    Base.metadata.drop_all(bind=engine)


def get_network_byID(jobID):
    network = db_session.query(Network).filter_by(id=jobID).first()

    return network


def add_network(dataset_name=None, email=None, data_prefix=None, half_life=None):
    job_start = datetime.utcnow()
    network = Network(dataset_name=dataset_name, email=email,
                      data_prefix=data_prefix, job_start=job_start,
                      half_life=half_life)
    db_session.add(network)
    db_session.commit()

    return network.id


def copy_network(job_id, data_prefix):

    nt = get_network_byID(job_id)

    new_network = Network(data_prefix=data_prefix, email=nt.email,
                          job_start=nt.job_start, status=nt.status,
                          job_end=nt.job_end, dataset_name=nt.dataset_name,
                          half_life=nt.half_life)

    db_session.add(new_network)
    db_session.commit()

    return new_network.id


def set_job_end(id=None, status=1):
    job_end = datetime.utcnow()
    update_network(id=id, job_end=job_end, status=status)


def update_network(network=None, id=None, **args):

    if network is None:   # load the network by id, then update it
        db_session.query(Network).filter_by(id=id).update(args)

    db_session.commit()     # just commit the changes
