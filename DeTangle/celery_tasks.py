from . import celery
import random
import time
from flask import current_app
from .model import database  as db
from .run_PEAK import run
import logging


log = logging.getLogger(__name__)

# -----------------------------------------------------------

@celery.task()
def submit_peak_job(job_id, network_prefix, data_path, half_life):

    log.info('------------- New Call to PEAK ----------------')
    log.info("Calling PEAK for job ID={}, and network_prefix={}".format(job_id, network_prefix))

    # set job as running
    db.set_job_end(job_id, status=2)
    status = -1  # error
    try:
        run(network_prefix, data_path, half_life=half_life)
        # Mark job as successfully done in the database
        status = 1
        msg = 'success'
    except OSError as e:
        msg = 'Input files format are invalid. Please check sample input.'
        log.error(e, exc_info=True)
    except Exception as e:
        log.error(e, exc_info=True)
        msg = 'An error occured while processing your data.'
    finally:
        # Save status in the DB
        db.set_job_end(job_id, status=status)

        # send email that job is ended with msg
        # TODO
        log.info(msg)

    log.info('-------------- PEAK id {} done (status = {}) --------------'
             .format(job_id, status))

    return status


@celery.task(bind=True)
def short_task(data):
    """Background task """
    app = current_app._get_current_object()
    print('My caller app config: ', app.config)

    print("Got data: =====> ", data)
    return data


@celery.task(bind=True)
def long_task(self):
    """Background task that runs a long function with progress reports."""

    print('ID: {}'.format(self.request.id))
    verb = ['Starting up', 'Booting', 'Repairing', 'Loading', 'Checking']
    adjective = ['master', 'radiant', 'silent', 'harmonic', 'fast']
    noun = ['solar array', 'particle reshaper', 'cosmic ray', 'orbiter', 'bit']
    message = ''
    total = random.randint(10, 50)
    for i in range(total):
        if not message or random.random() < 0.25:
            message = '{0} {1} {2}...'.format(random.choice(verb),
                                              random.choice(adjective),
                                              random.choice(noun))
        self.update_state(state='PROGRESS',
                          meta={'current': i, 'total': total,
                                'status': message})
        time.sleep(1)
        print(i)
    return {'current': 100, 'total': 100, 'status': 'Task completed!',
            'result': 42}

