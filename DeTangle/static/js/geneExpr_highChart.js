// $(function () {

function show_gene_expr(job_id, genes){
    // Get the CSV and create the chart

    // $.getJSON('https://www.highcharts.com/samples/data/jsonp.php?filename=analytics.csv&callback=?', function (csv) {
    // $.get("/geneExpr/"+job_id+"/ACR,SHR,MGP,JKD,PHB", function (csv) {
    $.get("/geneExpr/"+job_id+"/"+genes, function (csv) {

        //console.log('doneeee', csv);
        Highcharts.setOptions({
            chart: {
                style: {
                    fontFamily: 'Arial'
                }
            }
        });

        Highcharts.chart('geneExprDiv', {
           
            data: {csv: csv},
            // series: processed_json,

            title: {text: 'Gene Expression Data'},

            // subtitle: {text: 'Source: ...'},

            xAxis: {
                tickInterval: 1, //24 * 3600 * 1000, // one week
                tickWidth: 0,
                gridLineWidth: 1,
                // labels: {align: 'center', x:0, y: 15}
                title: {text: 'experiment number'},
            },

            yAxis: [
                { // left y axis
                    gridLineWidth: 0,
                    title: {text: 'Expression values'},
                    labels: {
                        align: 'right',
                        x: 0,
                        y: 0,
                        format: '{value:.,0f}'
                    },
                    showFirstLabel: false
                }, 
                // { // right y axis
                //     linkedTo: 0,
                //     gridLineWidth: 0,
                //     opposite: true,
                //     title: {
                //         text: null
                //     },
                //     labels: {
                //         align: 'right',
                //         x: 10,
                //         y: 16,
                //         format: '{value:.,0f}'
                //     },
                //     showFirstLabel: false
                // }
            ],

            legend: {
                align: 'left',
                verticalAlign: 'top',
                x: -10,
                y: 20,
                // floating: true,
                borderWidth: 0,
                // itemWidth: 30,
                layout: 'vertical',
                itemStyle: {
                    font: '8pt',
                    fontWeight: 'normal',
                },
                title: {
                    text: 'Gene name<br/><span style="font-size: 9px; color: #666; font-weight: normal">(Click to show/hide)</span>',
                    style: {fontStyle: 'italic'}
                },              
            },

            tooltip: {shared: false, crosshairs: true},

            plotOptions: {
                series: {
                    // showCheckbox: true,
                    cursor: 'pointer',                                 
                    states: {
                        hover: {enabled: true, lineWidth: 5}                                             
                    },                    
                    marker: {enabled: false},
                }
            },

            // series: [
            //     {
            //         name: 'All visits',
            //         lineWidth: 4,
            //         marker: {radius: 4}
            //     }, 
            //     {name: 'New visitors'}
            // ]
        });
    });
}
// });

