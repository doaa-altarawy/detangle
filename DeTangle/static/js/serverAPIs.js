// Communication with the server


// Messenger.options = {
//     extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
//     theme: 'flat'
// };

// specifies code that should run once the browser is done loading
// $(function(){
    // for flask and jquery
    //$SCRIPT_ROOT = {{ request.script_root|tojson|safe }};

    // prevent right click browser default on svgDisplay div (svg)
    // window.oncontextmenu = function (ev) {            
    //     ev.preventDefault();
    //     //return false;     // cancel default menu
    // }    
  
    function show_modal_js(title, msg){
        $('#msg_modal .modal-title').html(title);
        $('#msg_modal .modal-body').html(msg);
        $("#msg_modal").modal('show');
    }  

    function show_modal(data){
        $('div#formHolder').html(data);
        $("div#formHolder .modal").modal('show');
    }  

    var drawGraph = function(jsonGraph){
        var layout = {  // default layout
                name: 'cose',
                fit: true
        };
        if (jsonGraph.hasOwnProperty('fixPositions')){
            layout.name = 'present'; // fix node position
        }

        if (jsonGraph.hasOwnProperty('cyto_graph')){
            var cytoscapeJsGraph = {
                nodes: jsonGraph.cyto_graph.elements.nodes,
                edges: jsonGraph.cyto_graph.elements.edges,
                job_id: jsonGraph.job_id,
                pan: jsonGraph.cyto_graph.pan,
                zoom: jsonGraph.cyto_graph.zoom
            };
        }
        else{            
            var cytoscapeJsGraph = {
                edges: jsonGraph.edges,
                nodes: jsonGraph.nodes,
                layout: layout,
                job_id: jsonGraph.job_id
            };
        }
        console.log('----------------');
        console.log(cytoscapeJsGraph);

        refreshCytoscape(cytoscapeJsGraph);        
    };

    var updateGraph = function(jsonGraph){
        console.log('updateGraph with:')
        console.log(jsonGraph.edges);
        // find target nodes
        var targets = new Set();
        for (var i in jsonGraph.edges){
            targets.add(jsonGraph.edges[i].data.target);            
        }
        
        for (var i in jsonGraph.edges){
            var edge = jsonGraph.edges[i];
            var found = cy.edges('#'+edge.data.id);
            if (found.length > 0){
                if (found.hasClass('added')){
                    found.classes('new_pred');
                }else{
                    found.classes('new_old_pred')
                }
                found.data('label', edge.data.label);
                // console.log('Found: ', found.data());
            }else{
                found.classes('new_pred');
                cy.add(found);
                // console.log('Not found:', edge);
            }

            // if (cy.edges("[id="+ edge.data.id +"]")){
            //     cy.edges("[id="+ edge.data.id +"]")
            //         .addClass(edge.classes);
            // }else{
            //     cy.add(edge);
            // }
        }
        for (var target in targets){
            var edges = cy.nodes('#'+target).incomers().edges();
            for (var edge in edges){
                if (!edge.hasClass('new_pred') & !edge.hasClass('new_old_pred')){
                    console.log('removing-------', edge);
                    edge.remove();
                }
            }
        }        
    };

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // get Modal form for loading
    $('#load_job').on('click', function(){    
        console.log("load Job by ID...");    
        $.get("/_load_job_form", {form_id:'load_job_form'})
            .done(function(data) {
                // console.log(data);
                $('div#formHolder').html(data);
                $('#submitBtn').bind('click', load_job_graph);                       
                $("div#load_job_form").modal('show');
            })
            .fail(function() {
                console.log("A problem happened.");
            })
            .always(function() {
                //console.log("finished");
            });
        $('li.dropdown').removeClass('open');
        return false;           
    });

    // -----------------------------------------------------------

    var load_job_graph = function(e){

        $('#load_job_form form').validate({errorClass: 'error-class'});
        if  (! $('#load_job_form form').valid()){
            return false;
        }

        var formData = new FormData(document.querySelector("#load_job_form form"));
        
        job_id = $('[name="job_id"]').val();
        graphName = $("#dataset").val();
        count = Number($('#numOfEdges').val())
        //gold_standard = Number($('gold_standard:checked').val())
        gold_standard = Number($('input[name=gold_standard]:checked').val())
        console.log("job_id:"+ job_id +" graphName: "+graphName+ 
                    ", Count: "+count);
        $.ajax({
            type: "POST",
            url: "/_load_job_form",
            data: formData,           
            contentType: false, 
            processData: false,
            success: function(data){
                json_data = JSON.parse(data);
                console.log('Server data:');
                console.dir(json_data);

                if (json_data.sucess){
                    drawGraph(json_data);
                    setFileContent('Job ID: ' + job_id);
                    $('#geneExprDiv').html('');
                } else{
                    show_modal(json_data.msg);
                } 
            },
            fail: function(data){
                alert(data);               
            }
        });
        $("#load_job_form").modal('hide');
        return false;
        
    };

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Load sample saved jobs (examples) by job ID
    var load_sample_job = function(job_id){

        var formData = new FormData();
        
        formData.append('job_id', job_id);
       
        $.ajax({
            type: "POST",
            url: "/_load_job_form",
            data: formData,           
            contentType: false, 
            processData: false,
            success: function(data){
                json_data = JSON.parse(data);
                if (json_data.sucess){
                    drawGraph(json_data);
                    setFileContent('Job ID: ' + job_id);
                } else{
                    show_modal(json_data.msg);
                } 
            },
            fail: function(data){
                alert(data);               
            }
        });
    };

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // get Modal form for loading
    $('#loadGraphDialog').on('click', function(){    
        console.log("load Graph...");    
        $.get("/_loadDemoGraphList", {form_id:'chooseGraphForm'})
            .done(function(data) {
                // console.log(data);
                $('div#formHolder').html(data);
                // bind ok button action
                $('#submitBtn').bind('click', function(){
                        //var formData = new FormData($("#inputForm"));
                        graphName = $("#dataset").val();
                        count = Number($('#numOfGenes').val())
                        startNo = Number($('#startGene').val())
                        geneID = $('#geneID').val()
                        //gold_standard = Number($('gold_standard:checked').val())
                        gold_standard = Number($('input[name=gold_standard]:checked').val())
                        console.log("graphName: "+graphName+ 
                            ", From: "+startNo+" ,To: "+count+ ", geneID: "+ geneID);
                        getDrawGraph(graphName, startNo, count, gold_standard, geneID);    
                        setFileContent(graphName);                     
                        $("#chooseGraphForm").modal('hide');
                        return false;
                  });
                $("div#chooseGraphForm").modal('show');
            })
            .fail(function() {
                console.log("A problem happened.");
            })
            .always(function() {
                //console.log("finished");
            });
        $('li.dropdown').removeClass('open');
        return false;           
    });

    // //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // $('a#loadDemoGraph').on('click', function() {
    //     console.log("loadDemoGraph...");
    //     getDrawGraph('');
    //     // close dropdowns
    //     $('li.dropdown').removeClass('open');
    //     return false;
    // });
  
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // get Modal form for run data
    $('#runAutoTest').on('click', function(){        
        $.get("/_run_prediction", {formId:'inputForm'})
            .done(function(data) {
                //alert(data);
                $('div#formHolder').html(data);
                // bind ok button action
                $('#submitBtn').bind('click', submitForm);               
                $("div#inputForm").modal('show');
            })
            .fail(function() {
                alert( "A problem happened." );
            })
            .always(function() {
                //alert( "finished" );
            });            
    });

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // get and Draw graph!
    var submitForm = function(e){
        // e.preventDefault();

        $('#inputForm form').validate({errorClass: 'error-class'});
        if  (! $('#inputForm form').valid()){
            return false;
        }

        var formData = new FormData(document.querySelector("#inputForm form"));
        // formData.append('test', 'testttt');
        graphName = $("#dataset").val(); //not tested
        $("#inputForm").modal('hide');
        $.ajax({
            type: "POST",
            url: "/_run_prediction",
            data: formData, //$("#inputForm").serialize(), // serializes the form's elements.
            contentType: false, //'multipart/form-data',
            processData: false,
            success: function(data){
               // alert(data);     // show response from the server
               console.log(data);
               show_modal(data);
            },
            fail: function(){
                alert('failed!'); // show response from the server              
            }
        });       
        //getDrawGraph(graphName);
        return false;
    } 

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Load a ready graph from server
    function getDrawGraph(graphName){
        getDrawGraph(graphName, 0, 50, 1, '');
    }
    function getDrawGraph(graphName, start, count, gold_standard, geneID){
      $.getJSON('/_loadGraph', 
            {graphName: graphName,
              startGene: start,
              numOfGenes: count,
              gold_standard: gold_standard,
              geneID: geneID})
        .done(function(ntdata) {
            console.log('Server data:');
            console.dir(ntdata);
            drawGraph(ntdata);
        });
    }  

    function run_online_peak(){
    // Run online PEAK using modified edges

        deleted_edges = [];
        cy.edges('.deleted').forEach(function( ele ){
                        deleted_edges.push(ele.data());
                    });

        added_edges = [];
        cy.edges('.added').forEach(function( ele ){
                        added_edges.push(ele.data());
                    });

        console.log('deleted:', deleted_edges);
        console.log('added: ', added_edges);

       
        formData = new FormData();
        formData.append('job_id', cy.scratch('job_id'));
        formData.append('cyto_graph', JSON.stringify(cy.json()));
        formData.append('added_edges', JSON.stringify(added_edges));
        formData.append('deleted_edges', JSON.stringify(deleted_edges));
       
        console.log('Form Data:', formData);

        $.ajax({
            type: "POST",
            url: "/_online_peak",
            data: formData,           
            contentType: false, 
            processData: false,
            success: function(data){
                json_data = JSON.parse(data);
                console.log('Server response:');
                console.dir(json_data);

                if (json_data.sucess){
                    console.log('sucess');
                    clear_added_deleted();
                    updateGraph(json_data.cyto_graph);                   
                } 

                show_modal(json_data.msg);
            },
            fail: function(data){
                alert(data);               
            }
        });
    }
   
// }); //end anonymous func


function saveCytoToServer(save_copy){
    formData = new FormData();
    formData.append('job_id', cy.scratch('job_id'));
    formData.append('cyto_graph', JSON.stringify(cy.json()));
    formData.append('save_copy', (save_copy)?1:0);

    // console.log('FormData:---------')
    // // Display the key/value pairs
    // for (var pair of formData.entries()) {
    //     console.log(pair[0]+ ', ' + pair[1]); 
    // }

    $.ajax({
        type: "POST",
        url: "/_save_graph",
        data: formData,           
        contentType: false, 
        processData: false,
        success: function(data){
            json_data = JSON.parse(data);
            console.log('Server response:');
            console.dir(json_data);
            setFileContent('Job ID: ' + json_data.job_id);
            cy.scratch("job_id", json_data.job_id);

            if (json_data.sucess){
                show_modal(json_data.msg);
            } 
        },
        fail: function(data){
            alert(data);               
        }
    });
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




//========================================================
// Edge edge weight form 
function showTextBox(text){
    weightChanged = false;
    $('#edgeWeight').val(text);
    $('#editEdgeWeightForm').modal('show');
    $('#edgeWeightSubmit').bind('click', function(){
        var newWeight = Number($('#edgeWeight').val());
        console.log("New weight: "+newWeight);
        if (newWeight == NaN){
            alert(newWeight+ " is not a number. \nPlease" 
                + "Enter a number between 0 and 1");
            return;
        }
        updateWeight(newWeight);
        $("#editEdgeWeightForm").modal('hide');
        return true;
    });
}// end(showTextBox)

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// check if a mouse event is a right click
// function isRightClick(e) {
//     var isRightMB;
//     e = e || window.event;

//     if ("which" in e)  // Gecko (Firefox), WebKit (Safari/Chrome) & Opera
//         isRightMB = e.which == 3; 
//     else if ("button" in e)  // IE, Opera 
//         isRightMB = e.button == 2; 
//     return isRightMB;            
// } 