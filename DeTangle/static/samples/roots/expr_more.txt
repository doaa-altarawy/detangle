The expression data file contains the matrix of gene expression values. Each row corresponds to a microarray chip, and each column to a gene. In other words, element (i, j) is the expression value of gene j in chip i_. The first line of the file gives the unique gene name or label for every column.
The expression data should be already normalized with your preferred normalization method.

