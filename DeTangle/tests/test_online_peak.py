from nose.tools import assert_equal, assert_greater_equal, assert_almost_equal
from run_PEAK import run_online_peak
from model.database import get_network_byID
import pandas as pd
import os
import logging
import json

log = logging.getLogger(__name__)
logging.basicConfig(disable_existing_loggers=False,
                    level=logging.DEBUG,
                    format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s")


class TestOnlinePeak(object):

    def setup(self):
        self.job_id = 38
        self.data_prefix = get_network_byID(self.job_id).data_prefix
        self.added_edges = pd.read_json(open('../added.json', 'r'))
        self.deleted_edges = pd.read_json(open('../deleted.json', 'r'))
        self.path_data = '../uploads/'


    def test_empty_set(self):
        run_online_peak(self.job_id, self.data_prefix, self.path_data,
                        pd.DataFrame(), pd.DataFrame())

    def test_added_edges(self):
       run_online_peak(self.job_id, self.data_prefix, self.path_data,
                        self.added_edges, pd.DataFrame())

    def test_deleted_edges(self):
        pass

    def test_root_data(self):
        run_online_peak(self.job_id, self.data_prefix, self.path_data,
                        self.added_edges, self.deleted_edges)
