from flask import Flask
# from flask_sqlalchemy import SQLAlchemy
# from models.User import User

app = Flask(__name__)
# app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:doaaaa81@localhost/testAlchemy4' # mysql://username:password@server/db
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
# db = SQLAlchemy(app)


# class User(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     username = db.Column(db.String(80), unique=True)
#     email = db.Column(db.String(120), unique=True)
#
#     def __init__(self, username, email):
#         self.username = username
#         self.email = email
#
#     def __repr__(self):
#         return '<User %r>' % self.username


# db = SQLAlchemy()
# def create_app():
#     app = Flask(__name__)
#     db.init_app(app)
#     return app

# db.create_all()

from DBtest.database import db_session

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

