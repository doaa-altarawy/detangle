from nose.tools import assert_equal
from DBtest.database import db_session, init_db, drop_all
from DBtest.models import User



class Test_DB(object):

    @classmethod
    def setup_class(cls):
        init_db()

    @classmethod
    def teardown_class(cls):
        db_session.close()
        drop_all()

    def test_create_tables(self):
        admin = User('admin', 'admin@example.com')
        guest = User('guest', 'guest@example.com')
        db_session.add(admin)
        db_session.add(guest)
        db_session.commit()

        users = User.query.all()
        assert_equal(len(users), 2)

        admin = User.query.filter_by(name='admin').first()
        assert_equal(admin.name, 'admin')


# if __name__ == "__main__":
#     test = Test_DB()
#     test.setup_class()
#     test.test_create_tables()
#     # test.teardown_class()
