from nose.tools import assert_equal, assert_greater_equal, assert_almost_equal
from model import database as db
from model.models import Network
from datetime import datetime
import time


class TestDB(object):

    @classmethod
    def setup_class(cls):
        db.init_db()


    @classmethod
    def teardown_class(cls):
        db.db_session.close()
        # db.drop_all()

    def test_create_networks(self):
        db.add_network(dataset_name='test1', email='admin22222@example.com',
                                data_prefix=int(time.time()))
        id = db.add_network(email='guest@example.com',  data_prefix='sometext')

        networks = Network.query.all()
        assert_greater_equal(len(networks), 2)

        net = db.get_network_byID(id)
        assert_equal(net.email, 'guest@example.com')


    def test_update_network(self):

        id = db.add_network(email='admin@example.com',  data_prefix=int(time.time()))

        job_end = datetime.now()
        db.update_network(id=id, job_end=job_end)

        network = db.get_network_byID(id)
        assert_almost_equal(self._to_int(network.job_end), self._to_int(job_end), delta=3)

        id = db.add_network(email='admin2@example.com', data_prefix='prefix')

        db.set_job_end(id=id)

        network = db.get_network_byID(id)
        assert_greater_equal(self._to_int(network.job_end), self._to_int(network.job_start))


    def _to_int(self, date):
        return time.mktime(date.timetuple())

    # def test_update_network(self):
    #
    #     test_tf = 'test factor'
    #     test_clr = 'test CLR'
    #
    #     id = db.add_network('admin@example.com', None)
    #
    #     db.update_network(id=id, expression='new expression', trans_factor=test_tf)
    #
    #     network = db.get_network_byID(id)
    #     assert_equal(network.trans_factor, test_tf)
    #
    #     network.clr = test_clr
    #     db.update_network(network)
    #
    #     network = db.get_network_byID(id)
    #     assert_equal(network.clr, test_clr)


# if __name__ == "__main__":
#     test = Test_DB()
#     test.setup_class()
#     test.test_create_tables()
#     # test.teardown_class()
