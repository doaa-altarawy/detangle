__author__ = 'doaa'

import numpy as np
from timeit import timeit
import matplotlib.pyplot as plt


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Text Python arrays vs np arrays
def testTiming():
    setup1 = 'a = range(100)'
    setup2 = """
import numpy as np\n
a = np.arange(1000)
"""
    func1 = '[i**2 for i in a]'

    print("Python Arrays: {}".format(timeit(func1, setup1, number=1000000)))
    print("Numpy loop: {}".format(timeit(func1, setup2, number=1000000)))
    print("Numpy operator on object: {}".format(timeit('a**2', setup2, number=1000000)))

# print(help(np.array))

# Evenly spaced:
# b = np.arange(1, 9, 2) # start, end (exclusive), step ([1, 3, 5, 7])

# or by number of points:
# c = np.linspace(0, 1, 6)   # start, end, num-points
# array([ 0. ,  0.2,  0.4,  0.6,  0.8,  1. ])

# Common arrays:

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMP
def heatMapPlot():
    # mat = np.random.randn(30, 30)
    file = open("../testData/mixedCLR.txt","rb")
    mat = np.loadtxt(file, delimiter=" ", skiprows=0)
    print(mat)
    # normalPlot = plt.imshow(mat, cmap=plt.cm.hot)

    # heatmap = plt.pcolor(mat)
    fasterMap = plt.pcolormesh(mat)

    # Decorate the output
    plt.colorbar()
    plt.savefig("heatmap.png")
    print(mat.max(), mat.min())


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def generateData():
    """
        savetxt: save as txt
        save: to save as binary

        savetxt:
            x: ndarray
            names: the header array
            comments: by default a '#' preceed the header, use comments=''
            fmt: the format of the data
    """
    file = open("../testData/test.out", "wb")
    names = ['col0', 'col1', 'col2', 'col3', 'col4']
    x = np.arange(50).reshape(10, 5)
    # Write to file
    np.savetxt(file, x, header=' '.join(names), comments='',
               fmt='%f', delimiter=' ')
    file.close()
    # Check written data
    mat = np.loadtxt("../testData/test.out", delimiter=" ", skiprows=1)
    print(mat)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def readAsArrayOfLists():
    """genfromtxt:
        Reads the data as an array of rows. Each row is an array.
        Can access the columns by their names too.
        Can handle header and missing values.
        eg. mat[1][3]
    :return:
        numpy.ndarray: a list of records for each row if types are different
        or normal array of a single stype is given (eg '|U10')
    """
    file = open("../testData/test.txt","rb")
    mat = np.genfromtxt(file, delimiter=" ", skip_header=0, names=True,
                        replace_space='_', autostrip=True) # , dtype='S16')
    file.close()
    # Get header names
    print("Names: {}".format(mat.dtype.names))
    print("dim: {}".format(mat.ndim))   # 1
    print("Third row: {}".format(mat[2]))
    print("Second Col: {}".format(mat['col1']))
    print("Access First element: {}".format(mat[0][0])) #****


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def readAs2DArray():
    """loadtxt:
        Faster.
        Read the data as np 2D array.
        Cannot read headers or fill missing values (missing will be NAN)

    :return:
        numpy.array: 2D array
    """
    file = open("../testData/test.txt","rb")
    mat = np.loadtxt(file, delimiter=" ",skiprows=1) # skip header line
    file.close()
    print("dim: {}".format(mat.ndim))  # 2
    print("Third row: {}".format(mat[2,:]))
    print("Second Col: {}".format(mat[:, 1]))
    print("Access First element: {}".format(mat[0,0])) #****


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if __name__=='__main__':
    # testTiming()
    #  heatMapPlot()
    readAsArrayOfLists()
    # readAs2DArray()
    # generateData()