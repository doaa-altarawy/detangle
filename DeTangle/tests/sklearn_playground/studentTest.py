__author__ = 'doaa'

import numpy as np
from processInput import encodeCategorical
from sklearn import preprocessing
from sklearn import linear_model
from matplotlib import pyplot as plt
from sklearn.metrics import r2_score
import math

np.set_printoptions(precision=2, suppress=True)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def studentData():
    file = open("../testData/student/student-mat.csv","rb")

    mat = np.genfromtxt(file, delimiter=";", skip_header=0, dtype='|U20')
    file.close()
    header = mat[0, :]
    X_cat = mat[1:, 0:-3]
    Y = mat[1:, -3:].astype(float)
    categorical = ['school', 'sex', 'address', 'famsize', 'Pstatus', 'Mjob', 'Fjob',
                   'reason', 'guardian',  'schoolsup', 'famsup', 'paid',
                   'activities', 'nursery', 'higher', 'internet', 'romantic']
    non_cat_count = len(header) - len(categorical) - 3

   # nonzero returns tuple for all dim, so get first dim
    cat_inds = np.nonzero([(i in categorical) for i in header[:-3]])[0]

    X = encodeCategorical(X_cat, cat_inds)

    # The last non_cat_count columns need to be normalized
    X[:, -non_cat_count:] = preprocessing.scale(X[:, -non_cat_count:], axis=0)
    np.savetxt('../testData/student/x_encodeCat.csv',
               X,  fmt='%f', delimiter='\t')
    print("Mean: \n", X.mean(axis=0))
    print("Var: \n", X.std(axis=0))


    # ------------------------
    n_samples = X.shape[0]
    n_test = n_samples/4

    X_train = X[:-n_test]
    X_test = X[-n_test:]
    Y_train = Y[:-n_test]
    Y_test = Y[-n_test:]

    linearModel = linear_model.LinearRegression()
    linearModel.fit(X_train, Y_train)
    Y_predict = linearModel.predict(X_test)
    print(Y_predict.shape)
    print("R2 score (best is 1): ", r2_score(Y_test, Y_predict))

    # Save predictions side by side
    np.savetxt('../testData/student/Y_predict.csv',
               np.concatenate((Y_test, Y_predict), axis=1),
               fmt='%d', delimiter='\t')
    np.savetxt('../testData/student/coeff.csv',
               linearModel.coef_,
               fmt='%1.2f', delimiter='\t')

    # Plotting
    # ----------
    t = np.arange(1, n_test)
    diff = np.absolute(Y_test-Y_predict)
    plt.scatter(t, diff[:,0],  color='blue')
    plt.savefig('../testData/student/studentRegression.png')


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def simpleData():
    file = open("../testData/simple1.csv","rb")

    mat = np.genfromtxt(file, delimiter=",", skip_header=0, dtype='|U20')
    file.close()
    header = mat[0, :]
    X = mat[1:, 0:-1].astype(float)
    Y = mat[1:, -1:].astype(float)


    # normalized
    # X = preprocessing.scale(X, axis=0)
    X /= X.std(axis=0)  # Standardize data (from sklearn examples)

    # ------------------------
    n_samples = X.shape[0]
    n_test = n_samples/4

    X_train = X[:-n_test]
    X_test = X[-n_test:]
    Y_train = Y[:-n_test]
    Y_test = Y[-n_test:]

    linearModel = linear_model.LinearRegression()
    linearModel.fit(X_train, Y_train)
    Y_predict = linearModel.predict(X_test)



    # The mean square error
    print(np.mean(Y_predict - Y_test))
    print("Residual sum of squares: {:.2f}"
          .format(np.mean(Y_predict - Y_test) ** 2))
    # Explained variance score: 1 is perfect prediction
    print('Variance score: {:.2f}'
          .format(linearModel.score(X_test, Y_test)))
    # --> same as:
    print("R2 score (best is 1): ", r2_score(Y_test, Y_predict))

    # Plotting
    # ----------
    plt.scatter(X_train, Y_train,  color='blue')
    plt.scatter(X_test, Y_test,  color='red')
    plt.plot(X, linearModel.predict(X),  color='green', linewidth=2)
    plt.savefig('../testData/simpleRegression.png')


if __name__ == "__main__":
    simpleData()