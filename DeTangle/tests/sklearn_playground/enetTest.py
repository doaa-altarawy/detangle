"""
========================================
 Elastic Net test
========================================

Estimates Elastic-Net regression models on a manually generated
sparse signal corrupted with an additive noise. Estimated coefficients are
compared with the ground-truth.

"""

import numpy as np
import matplotlib.pyplot as plt

from sklearn.metrics import r2_score
from sklearn.linear_model import ElasticNet, ElasticNetCV
# from sklearn2.linear_model import ElasticNet as ElasticNetWeighted

###############################################################################
def main():
    # generate some sparse data to play with
    np.random.seed(50)

    n_samples, n_features = 100, 100
    X = np.random.randn(n_samples, n_features)
    coef = 3 * np.random.randn(n_features)
    inds = np.arange(n_features)
    np.random.shuffle(inds)
    nonZeroCount = 20
    coef[inds[nonZeroCount:]] = 0  # sparsify coef
    y = np.dot(X, coef)
    nonZeroCoef_inx = inds[0:nonZeroCount]

    # for i in nonZeroCoef_inx:
    #         print(i, coef[i])

    # add noise
    y += 1 * np.random.normal((n_samples,))

    # no need to normalize: --> data generated from Normal, already have var~1
    # X /= X.std(axis=0)

    # Split data in train set and test set
    n_samples = X.shape[0]
    X_train, y_train = X[:n_samples / 2], y[:n_samples / 2]
    X_test, y_test = X[n_samples / 2:], y[n_samples / 2:]

    ###############################################################################
    # ElasticNet

    # Feature scalling as prior knowledge
    ###############################################
    weight = np.ones(X.shape[1])
    weight[nonZeroCoef_inx[:nonZeroCount*0.5]] = .1

    # X_train = np.divide(X_train, weight)
    # X_test = np.divide(X_test, weight)
    ###############################################

    alpha = 0.1
    # enet = ElasticNet(alpha=alpha, l1_ratio=0.7)
    enet = ElasticNet(alpha=alpha, l1_ratio=0.7, l1_weights=weight)
    # enet = ElasticNetCV(l1_ratio=0.7, l1_weights=weight)

    enet.fit(X_train, y_train)

    coef_pred = enet.coef_ #np.divide(enet.coef_, weight)   ###### rescale

    getPredictionAccurMeasures(nonZeroCoef_inx, coef_pred, 0.001)

    y_pred = enet.predict(X_train)
    score_train = r2_score(y_train, y_pred)
    print("r^2 on Train data : ", score_train)

    y_pred = enet.predict(X_test)
    score_test = r2_score(y_test, y_pred)
    print("r^2 on test data : ", score_test)


    plt.figure(2)
    plt.plot(coef_pred, label='Elastic net coefficients')
    plt.plot(coef, '--', label='original coefficients')
    plt.legend(loc='best')
    plt.title("Elastic Net R^2: {}".format(score_test))

    plt.savefig("enetCoefPlot.png")

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def runPrintResults(X, y, alpha, name):

    print(name+":\n=========")

    if (alpha is not None):
        X_new = np.divide(X, alpha)
    else:
        X_new = X

    enetCV = ElasticNetCV(l1_ratio=0.8, fit_intercept=False) # cv=nCV, max_iter=5000
    # enetCV = LassoCV(fit_intercept=False) # cv=nCV, max_iter=5000

    enetCV.fit(X_new, y)
    y_pred_enet = enetCV.predict(X_new)
    r2_score_enet = r2_score(y, y_pred_enet)
    print("R2= ", r2_score_enet)


    if (alpha is not None):
        enetCV_coef = np.divide(enetCV.coef_, alpha)
    else:
        enetCV_coef = enetCV.coef_

    print("Best Alpha: {}".format(enetCV.alpha_))
    # print("coefs_: {}".format(enetCV.coef_))
    print("coefs_/alpha: {}".format(enetCV_coef))

    return enetCV.alpha_, enetCV_coef

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def getPredictionAccurMeasures(trueFeatures_inx, predFeaturs_coef, threshold=0.1, printResults=True):
    """
    :param trueFeatures: array of the indices of correct features
    :param predFeaturs: predicted values of the features (coeff)
    :param threasould: threshould to consider a feature = 0
    :return: TP, FP, FN
    """
    nonZero_Pred_inx = np.where(np.abs(predFeaturs_coef) > threshold)[0]

    TP = np.intersect1d(trueFeatures_inx, nonZero_Pred_inx).size
    FP = nonZero_Pred_inx.size - TP
    FN = trueFeatures_inx.size - TP

    if (printResults):
        print("True +ve(correct): ", TP, "\nFalse +ve(wrong Pred)", FP,
              "\nFalse -ve (missed): ", FN)

    return TP, FP, FN

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def testDifferentAlphas(X_train, y_train, X_test, y_test):
    alphas = [0.001, 0.1, 0.3, 0.5, 0.7, 0.9]
    score_test = np.zeros(len(alphas))
    score_train = np.zeros(len(alphas))

    for i,alpha in enumerate(alphas):
        # alpha = 0.1
        enet = ElasticNet(alpha=alpha, l1_ratio=0.7)
        enet.fit(X_train, y_train)


        y_pred = enet.predict(X_train)
        score = r2_score(y_train, y_pred)
        # print("r^2 on Train data : ", score)
        score_train[i] = score

        y_pred = enet.predict(X_test)
        score = r2_score(y_test, y_pred)
        # print("r^2 on test data : ", score)
        score_test[i] = score

    # For CV
    enet = ElasticNetCV(l1_ratio=0.7)
    enet.fit(X_train, y_train)
    print("CV alpha: ", enet.alpha_, " CV l1_ratio: ", enet.l1_ratio_)

    y_pred = enet.predict(X_train)
    scoreCV_train = r2_score(y_train, y_pred)

    y_pred = enet.predict(X_test)
    scoreCV_test = r2_score(y_test, y_pred)


    plt.figure(1)
    plt.plot(alphas, score_train, label="Train accur")
    plt.plot(alphas, score_test, label="Test accur")
    plt.scatter(enet.alpha_, scoreCV_train, label="CV Train accur")
    plt.scatter(enet.alpha_, scoreCV_test, label="CV Test accur")

    plt.legend(loc='best')
    plt.savefig("alpha_accur.png")

if __name__ == "__main__":
    main()