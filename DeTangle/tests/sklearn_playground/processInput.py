__author__ = 'doaa'

from sklearn import preprocessing
import numpy as np

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# set pretty printing
# set precision, and suppress scientific notation for small numbers
np.set_printoptions(precision=2, suppress=True)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def scaleDataset(X):
    print("X=\n", X)

    x_scaled = preprocessing.scale(X, axis=0)
    print("x_scaled= \n",x_scaled)

    print("Mean: \n", x_scaled.mean(axis=0))
    print("Var: \n", x_scaled.std(axis=0))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def decToBin():
    X = [[ 1., -1.,  2.], [ 2.,  0.,  0.], [ 0.,  1., -1.]]

    binarizer = preprocessing.Binarizer(threshold=0.5)

    result = binarizer.transform(X)
    '''Out:
    array([[ 1.,  0.,  1.],
           [ 1.,  0.,  0.],
           [ 0.,  1.,  0.]])
    '''
    print(result)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def encodeCategorical(x_cat, columns=None, zeroOne=True):
    '''
        Expand categorical features into n features, where n is
        number of possible values.
        Returns the transformed cat features and first columns, then
        non-categorical in the end.
        dtype=float
    '''
    # 1- Discritize str labels into numbers
    # If no comuns specified take all column indices
    if (columns is None):
        columns = [i for i in range(x_cat.shape[1])]

    encoders = {i:preprocessing.LabelEncoder() for i in columns}

    [encoders[i].fit(x_cat[:, i]) for i in encoders.keys()]

    x_numeric = np.zeros(x_cat.shape)
    for i in range(x_cat.shape[1]):   # for all columns
        if (i in columns):            # if feature is categorical, transform it
            x_numeric[:, i] = encoders[i].transform(x_cat[:, i])
        else:
            x_numeric[:, i] = x_cat[:, i]

    # 2- Expand each category into n features, where n is possible values
    encoder = preprocessing.OneHotEncoder(categorical_features=columns)
    x_final = encoder.fit(x_numeric).transform(x_numeric).toarray()

    return x_final


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if __name__=='__main__':
    X = np.arange(24, dtype=float).reshape(6, 4)
    scaleDataset(X)
    x_norm = preprocessing.normalize(X, axis=0)
    print(x_norm)
    # Make max 1, assume X has float
    X_max1 = X / np.max(X)
    # X = np.random.randn(4, 5)
    # scaleDataset(X)

    # dtype=object to accept mixed types (~ R, Pandas DataFrame)
    x_cat = np.array([[333, 'Male', 32, 'Alex', .4],
            [222, 'Male', 54, 'Cairo', .7],
            [555, 'Female', 41, 'Aswan', .3]], dtype=object)
    x_cat.dtype.names = ('c1', 'c2', 'c3', 'c4', 'c5')
    cat_col_indx = [1, 3]
    x = encodeCategorical(x_cat, cat_col_indx)
    print(x)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# -------------------------------------
#               Tests
# -------------------------------------

def labelEncoderExample():
    a = ["paris", "paris", "tokyo", "amsterdam"]
    encoders = preprocessing.LabelEncoder()
    encoders.fit(a)

    x = np.array([["tokyo", '1'],
                ["paris", '4']])
    b = np.zeros(x.shape)
    b[:, 0] = encoders.transform(x[:, 0])
    b[:, 1] = x[:, 1].astype(float)
    print(b)
    # array([2, 2, 1]...)
    s = encoders.inverse_transform([2, 2, 1])
    print(s)
    # ['tokyo', 'tokyo', 'paris']

def encodeCategoricalExample():
    b = [['1', '2'],
         ['0', '1']]
    enc = preprocessing.OneHotEncoder()
    # x = enc.fit(a).transform(a).toarray()   # 4 x 9
    x = enc.fit_transform(b).toarray()
    print("x: ", x.shape)
    enc.transform([[0, 1, 3]]).toarray()
    print(enc.categorical_features)
