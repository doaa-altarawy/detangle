"""
=====================
Lasso and Elastic Net
=====================

Lasso and elastic net (L1 and L2 penalisation) implemented using a
coordinate descent.

The coefficients can be forced to be positive.
"""
#print(__doc__)

# Author: Alexandre Gramfort <alexandre.gramfort@inria.fr>
# License: BSD 3 clause

import numpy as np
import matplotlib.pyplot as plt
from pprint import pprint

from sklearn.linear_model import lasso_path, enet_path, ElasticNetCV
from sklearn import datasets
from sklearn.metrics import r2_score


np.set_printoptions(formatter={'float': '{: 0.2f}'.format})

def lassoElasticnetPaths(X, y):
    # Compute paths

    eps = 5e-3   # the smaller it is the longer is the path
    alphas = np.arange(2, 4, 0.2)
    alphas = np.append(alphas, 2.27889) # best aplpha from cv

    # Computing regularization path using the lasso
    alphas_lasso, coefs_lasso, _ = lasso_path(X, y, eps, fit_intercept=False,
                                              alphas=alphas)

    # Computing regularization path using the positive lasso
    alphas_positive_lasso, coefs_positive_lasso, _ = lasso_path(
        X, y, eps, positive=True, fit_intercept=False, alphas=alphas)
    # Computing regularization path using the elastic net
    alphas_enet, coefs_enet, _ = enet_path(
        X, y, eps=eps, l1_ratio=0.8, fit_intercept=False, alphas=alphas)

    # Computing regularization path using the positve elastic net
    alphas_positive_enet, coefs_positive_enet, _ = enet_path(
        X, y, eps=eps, l1_ratio=0.8, positive=True, fit_intercept=False, alphas=alphas)

    # ElasticnetCV
    enetCV = ElasticNetCV(l1_ratio=0.8, fit_intercept=False) # cv=nCV, max_iter=5000
    enetCV.fit(X, y)
    coefs_enetCV = enetCV.coef_
    # repeat alpha for x-axis values for plotting
    alpha_enetCV = [enetCV.alpha_] * coefs_enetCV.shape[0]

    print("Best Alpha: {}".format(enetCV.alpha_))
    print("coefs_enetCV: {}".format(coefs_enetCV))
    print("coefs_enet: {}".format(coefs_enet[:, -1]))
    print("coefs_lasso: {}".format(coefs_lasso[:, -1]))

    y_pred_enet = enetCV.predict(X)
    r2_score_enet = r2_score(y, y_pred_enet)
    print("R2= ", r2_score_enet)

     # Display results

    plt.figure(1)
    ax = plt.gca()
    ax.set_color_cycle(2 * ['b', 'r', 'g', 'c', 'k'])
    l1 = plt.plot(alphas_lasso, coefs_lasso.T)
    l2 = plt.plot(alphas_enet, coefs_enet.T, linestyle='--')

    l3 = plt.scatter(alpha_enetCV, coefs_enetCV, marker='x')

    plt.xlabel('alpha')
    plt.ylabel('coefficients')
    plt.title('Lasso and Elastic-Net Paths')
    plt.legend((l1[-1], l2[-1]), ('Lasso', 'Elastic-Net'),
                loc='upper right')
    plt.axis('tight')
    plt.savefig("fig/lassoEnet")


    plt.figure(2)
    ax = plt.gca()
    ax.set_color_cycle(2 * ['b', 'r', 'g', 'c', 'k'])
    l1 = plt.plot(alphas_lasso, coefs_lasso.T)
    l2 = plt.plot(alphas_positive_lasso, coefs_positive_lasso.T,
                  linestyle='--')

    plt.xlabel('alpha')
    plt.ylabel('coefficients')
    plt.title('Lasso and positive Lasso')
    plt.legend((l1[-1], l2[-1]), ('Lasso', 'positive Lasso'), loc='upper right')
    plt.axis('tight')
    plt.savefig("fig/lassoPostEnet")


    plt.figure(3)
    ax = plt.gca()
    ax.set_color_cycle(2 * ['b', 'r', 'g', 'c', 'k'])
    l1 = plt.plot(alphas_enet, coefs_enet.T)
    l2 = plt.plot(alphas_positive_enet, coefs_positive_enet.T,
                  linestyle='--')

    plt.xlabel('alpha')
    plt.ylabel('coefficients')
    plt.title('Elastic-Net and positive Elastic-Net')
    plt.legend((l1[-1], l2[-1]), ('Elastic-Net', 'positive Elastic-Net'),
               loc='upper right')
    plt.axis('tight')
    plt.savefig("fig/lassoEnetPost")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if __name__ == "__main__":
    diabetes = datasets.load_diabetes()
    pprint(diabetes)
    print("Size of data:{}".format(diabetes.data.shape))
    X = diabetes.data
    y = diabetes.target

    X /= X.std(axis=0)  # Standardize data (easier to set the l1_ratio parameter)
    lassoElasticnetPaths(X, y)