"""
Record arrays (aka “Structured arrays”):
subclass of numpy.array
- Can have different data types
- columns can have names
- allows field access using attributes.

One downside of recarrays is that the attribute access feature
slows down all field accesses??

Similar to R and pandas DataFrame
"""

import numpy as np

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Docs example:
print("Docs example:\n--------------\n")
# Create an array with two fields, x and y:
recArr = np.array([(1.0, 2, 5.5),     # must be tuples
                   (3.0, 4, 2.3),
                   (0.2, 7, 7.1)],
            dtype=[('x', 'f4'),
                   ('y', 'i4'),
                   ('z', 'f4')])  # giving more than one type make it Rec Array
# print(a.x) -> wrong
# print(a[0, 0]) -> wrong

print("a['x']: ", recArr['x'])   # first col as array
print(recArr[0][0]) # ok
print("recArr[1]:", recArr[1])    # second row as tuple

# Get views:
r = recArr.view(np.recarray)
print("r.x:", r.x)      # first col as array
print("r.y:", r.y)
print("r[0][0]:", r[0][0])

# get regular array view for SIMILAR types:
arr = recArr[['x', 'z']].view('f4').reshape(recArr.shape[0], 2)
print("arr:", arr)
print(arr[0,0])



