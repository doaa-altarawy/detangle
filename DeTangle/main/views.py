from __future__ import division, print_function, absolute_import

import os, sys

# work around to be able to run this script as a module in development
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from flask import request, jsonify, redirect, flash, current_app
from flask import render_template, send_from_directory, url_for

# from flask_debugtoolbar import DebugToolbarExtension
import json
import logging
import time
import subprocess
import pandas as pd
from os.path import join
import shutil, glob

from ..netAnalysis.utils import InOut as IO
from ..netAnalysis.utils import Datasets as ds
from ..netAnalysis.utils import validator
from ..model import database as db
from ..run_PEAK import run_online_peak
from . import main
from ..celery_tasks import short_task, long_task, submit_peak_job


sys.path.append(os.path.dirname(__file__))

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


ALLOWED_EXTENSIONS = {'fq', 'txt'}

# toolbar = DebugToolbarExtension(app)

# Not working from here, only from the wsgi file
# logging.basicConfig(
#                     level=logging.DEBUG,
#                     format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s")

log = logging.getLogger(__name__)

DEMO_EXAMPLES = [11, 35, 60, 61]  # read only examples

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@main.route('/longtask')
def longtask():
    task = long_task.apply_async()
    return redirect(url_for('main.taskstatus', task_id=task.id))

@main.route('/status/<task_id>')
def taskstatus(task_id):
    print('Task id::: ', task_id)
    task = long_task.AsyncResult(task_id)
    if task.state == 'PENDING':
        response = {
            'state': task.state,
            'current': 0,
            'total': 1,
            'status': 'Pending...'
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
            'current': task.info.get('current', 0),
            'total': task.info.get('total', 1),
            'status': task.info.get('status', '')
        }
        if 'result' in task.info:
            response['result'] = task.info['result']
    else:
        # something went wrong in the background job
        response = {
            'state': task.state,
            'current': 1,
            'total': 1,
            'status': str(task.info),  # this is the exception raised
        }
    return jsonify(response)


@main.route("/h")
def test_hello_async():
    # flash('Hello')

    #TODO:
    short_task.apply_async(args=[{'msg': 'Hello from Flask'}])

    return redirect(url_for('static', filename='index.html'))


@main.route("/")
def index():
    # flash('Hello')
    # return render_template("index.html")
    return redirect(url_for('static', filename='index.html'))
#     #### return current_appsend_static_file('index.html')

# @app.route("/")
# def index():
#     # return render_template("index.html")
#     print('Nowww: ', os.getcwd())
#     # return redirect('/cytoscape/public_html/index.html')
#     return current_appsend_static_file('/cytoscape/public_html/index.html')

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@main.route('/_loadGraph', methods=['GET', 'POST'])
def loadGraph():
    log.info("Starting load graph........")
    numOfGenes = 50
    startGene = 0
    datasetName = 'Net1'
    gold_standard = 1
    geneID = ''
    flash("Loading graph...")
    # log.info(request.args)
    try:
        datasetName = request.args.get('graphName')
        gold_standard = int(request.args.get('gold_standard'))
        numOfGenes = int(request.args.get('numOfGenes'))
        startGene = int(request.args.get('startGene'))
        geneID = request.args.get('geneID')
    except Exception as e:
        # errmsg = "Error: {}".format(sys.exc_info()[0])
        errmsg = "Problem when parsing input data: ".format(e)
        log.error(errmsg)
        flash(errmsg)

    if (startGene  is None):
        startGene = 0
    if (numOfGenes is None):
        numOfGenes = 50
    if (gold_standard is None):
        gold_standard = 1

    # Send a msg to Flask
    # flash("Chosen datasetName: {}".format(datasetName))
    log.info("Dataset: {}\t numOfGenes: {}\t startGene:{} \tGeneID:{}"
             .format(datasetName, numOfGenes, startGene, geneID))
    # Return the graph as JSON
    if (gold_standard):
        fileName = ds.datasets[datasetName].pathToData +\
                ds.datasets[datasetName].goldStd
    else:
        fileName = ds.datasets[datasetName].pathToData +\
                ds.datasets[datasetName].outFile()
    log.info("Filename: {}".format(fileName))

    # return IO.readGraphFileAsJSON1(fileName, maxLines=numOfGenes,
    #                         sep="\t", startLine=startGene, geneID=geneID)

    json_data = IO.readGraphFileAsJSON_cyto(fileName, maxLines=numOfGenes,
                                   sep="\t", startLine=startGene, geneID=geneID)
    return json.dumps(json_data)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@main.route('/uploads/<path:filename>')
def download_file(filename):
    return send_from_directory(current_app.config['UPLOAD_FOLDER'],
                               filename, as_attachment=True)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@main.route('/geneExpr/<job_id>/<genes>', methods=['GET'])
def get_gene_expression(job_id, genes):
    print('Find gene expression for genes: ', genes)
    gene_list = genes.split(',')

    if (len(gene_list) == 0):
        return ''

    network = db.get_network_byID(job_id)
    log.info('Loaded network: {}'.format(network))
    fileName = join(current_app.config['UPLOAD_FOLDER'], network.data_prefix + '_expr.csv')
    expr = pd.read_csv(fileName, sep='\t')
    print (expr.head())
    expr = expr.loc[:, gene_list]
    expr /= expr.std(axis=0)
    print (expr.head())
    return expr.to_csv()
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@main.route('/_loadDemoGraphList', methods=['GET'])
def loadDemoGraphList():
    form_id = request.args.get('form_id')
    return render_template('loadGraph_form.html',
                               formID = form_id,
                               datasets = ds.DREAM5_datasets,
                               okBtn = 'Draw Graph') # load empty form

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


@main.route('/_download_job_csv')
def download_job_csv():
    """Download job results by ID as csv file"""

    job_id = request.args.get('job_id')

    log.info('Getting CSV file for Job ID: -------- {}'.format(job_id))
    network = db.get_network_byID(job_id)

    log.info('Loaded network: {}'.format(network))

    if not network:
        return json.dumps({'sucess': False,
                           'msg': render_template('message_form.html',
                                  title='Error',
                                  message=['Load a Job first (using File->Load job by ID) to be able to download it as csv.'])
                            })

    # Job exists and finished
    data_prefix = network.data_prefix

    # Return the graph as JSON
    # check if a cyto graph is saved
    result = {}

    fileName = data_prefix + '_out.csv'
    log.debug("Found and returning Filename: {}".format(fileName))

    result['sucess'] = True
    result['job_id'] = job_id
    with open(join(current_app.config['UPLOAD_FOLDER'], fileName), 'r') as fp:
        content = fp.read()
    result['data'] = content

    # return send_from_directory(current_app.config['UPLOAD_FOLDER'],
    #                            fileName,
    #                            as_attachment=True, attachment_filename=fileName)

    return json.dumps(result)


@main.route('/_load_job_form', methods=['GET', 'POST'])
def load_job_form():
    """Load job results by ID"""

    if request.method == 'GET':
        form_id = request.args.get('form_id')
        return render_template('load_job_form.html',
                           formID = form_id,
                           datasets = ds.DREAM5_datasets,
                           okBtn = 'Load Job')


    # request.method == 'POST'
    # -----------------------
    log.info("Starting load graph........")

    flash("Loading graph...")
    log.debug(request.form)

    job_id = str(request.form.get('job_id'))
    # datasetName = request.form.get('graphName')
    # gold_standard = request.form.get('gold_standard')
    numOfEdges = request.form.get('numOfEdges')
    target_genes = request.form.get('targetGenes').strip()

    if len(target_genes) > 0:
        target_genes = target_genes.split(',')
        target_genes = [s.strip() for s in target_genes]

    if numOfEdges:
        numOfEdges = int(numOfEdges)

    log.info('Job ID: -------- {}'.format(job_id))
    network = db.get_network_byID(job_id)

    log.info('Loaded network: {}'.format(network))

    if not network:
        return json.dumps({'sucess': False,
                           'msg': render_template('message_form.html',
                                  title='Error',
                                  message=['Job ID "{}" does not exist'.format(job_id)])
                            })
    elif network.status == 0:
        return json.dumps({'sucess': False,
                            'msg': render_template('message_form.html',
                                   title='Job Queue',
                                   message=['Job ID "{}" is still in the Queue and has not started yet.'.format(job_id)])
                            })

    elif network.status == 2:
        return json.dumps({'sucess': False,
                            'msg': render_template('message_form.html',
                                   title='Job Running',
                                   message=['Job ID "{}" is still running'.format(job_id)])
                            })
    elif network.status == -1:
        return json.dumps({'sucess': False,
                       'msg': render_template('message_form.html',
                                              title='Job Failed',
                                              message=['An error occured while processing '+\
                                                       'your input files for Job ID "{}". '
                                                       'Please review the sample file formats and their description files.'
                                                        .format(job_id)])
                       })

    # Job exists and finished
    data_prefix = network.data_prefix

    # Return the graph as JSON
    # check if a cyto graph is saved
    result = {}
    log.info('--------Data predix={} , job_id={}'.format(job_id, data_prefix))

    cyto_fileName = join(current_app.config['UPLOAD_FOLDER'], data_prefix + '_cyto.csv')
    fileName = join(current_app.config['UPLOAD_FOLDER'], data_prefix + '_out.csv')
    if len(target_genes) > 0:
        log.info('loading subgraph for genes {}'.format(target_genes))
        result = IO.readGraphFileAsJSON_cyto(fileName, geneID=target_genes)
    elif os.path.isfile(cyto_fileName): # use cyto file
        with open(cyto_fileName, 'r') as file:
            result['cyto_graph'] = json.load(file)
            # log.debug(result)
    else: # use output file
        log.debug("Filename: {}".format(fileName))
        result = IO.readGraphFileAsJSON_cyto(fileName, maxLines=numOfEdges)

    result['sucess'] = True
    result['job_id'] = job_id

    if 'nodes' in result and len(result['nodes']) == 0:
        return json.dumps({'sucess': False,
                            'msg': render_template('message_form.html',
                                   title='No Genes found',
                                   message=['Target Gene IDs are not found, '
                                            'please make sure to use the same gene Ids as in the '
                                            'gene Expression file (case senstive), and to seperate gene IDs with commas'])
                            })

    return json.dumps(result)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@main.route('/_save_graph', methods=['POST'])
def save_graph():

    job_id = request.form.get('job_id')
    save_copy = request.form.get('save_copy')


    if int(job_id) in DEMO_EXAMPLES:
        log.info('Cloning the Demo example {}'.format(job_id))
        job_id = _clone_job(job_id)
    elif int(save_copy) == 1:
        log.info('Saving a COPY of {}'.format(job_id))
        job_id = _clone_job(job_id)

    cyto_graph = request.form.get('cyto_graph')

    network = db.get_network_byID(job_id)
    if not network:
        return 'error'

    fileName = join(current_app.config['UPLOAD_FOLDER'], network.data_prefix + '_cyto.csv')

    with open(fileName, 'w') as outfile:
        outfile.write(cyto_graph)

    return json.dumps({'sucess': True,
                       'job_id': job_id,
                       'msg': render_template('message_form.html',
                                              title='Network saved',
                                              message=['Network was saved to server sucessfully.',
                                                       'Job ID "{}".'.format(job_id)])
                       })

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def _clone_job(job_id):
    """Clone the given job ID
     -  save data files
     - save the job to the database

    :returns new job Id
    """
    network = db.get_network_byID(job_id)
    data_prefix = network.data_prefix

    new_data_prefix = str(int(time.time() * 100))

    # For every data file for this source job
    for file in glob.glob(join(current_app.config['UPLOAD_FOLDER'], data_prefix + "*")):
        suffix = file.split(data_prefix)[-1]
        log.info('Suffix: {}'.format(suffix))
        shutil.copy2(file, join(current_app.config['UPLOAD_FOLDER'], new_data_prefix+suffix))

    new_job_id = db.copy_network(data_prefix=new_data_prefix, job_id=job_id)

    log.info('Database job created with id={}, data_prefix={}'
             .format(new_job_id, new_data_prefix))

    return new_job_id

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@main.route('/_online_peak', methods=['POST'])
def online_peak():
    """Run online PEAK with the modified graph"""

    result = {}
    job_id = request.form.get('job_id')
    cyto_graph = request.form.get('cyto_graph')
    deleted_edges = request.form.get('deleted_edges')
    added_edges = request.form.get('added_edges')

    # with open('deleted.json', 'w') as fp:
    #     fp.write(deleted_edges)
    #
    # with open('added.json', 'w') as fp:
    #     fp.write(added_edges)

    deleted_edges = pd.read_json(deleted_edges)
    added_edges = pd.read_json(added_edges)

    # log.debug('Added Edges: {}'.format(added_edges))
    # log.debug('Deleted Edges: {}'.format(deleted_edges))


    network = db.get_network_byID(job_id)
    if not network:
        result['sucess'] = False
        result['msg'] = render_template('message_form.html',
                                        title='Network Error',
                                        message=['Job ID is not found!'])

        return json.dumps(result)

    fileName = run_online_peak(job_id, network.data_prefix,
                                 current_app.config['UPLOAD_FOLDER'],
                                 added_edges, deleted_edges)

    cyto_graph = IO.readGraphFileAsJSON_cyto(fileName, mark_new=True)

    result['sucess'] = True
    result['msg'] = render_template('message_form.html',
                                     title='Network re-evaluation',
                                     message=['Network was updated sucessfully.'])
    result['cyto_graph'] = cyto_graph

    return json.dumps(result)



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#           post-redirect-get method:
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@main.route('/_run_prediction', methods=['GET', 'POST'])
def run_prediction():
    """
    Predict the GRN using the uploaded data asyncrounusly
    """

    if request.method == 'POST':

        log.info('POST is called')
        log.debug('Form: {}'.format(request.form))
        # log.debug('Files: {}'.format(request.files))

        email = request.form.get('user_email')
        dataset_name = request.form.get('dataset_name')
        is_timeseries = request.form.get('is_timeseries')
        half_life = request.form.get('half_life')
        if half_life:
            half_life = float(half_life)
        else:
            half_life = None

        # Send a msg to Flask
        # flash("FileUploaded: {}, outFilename={}".format(file, outFilename))

        # Read uploaded files
        expr_file = request.files.get('expr_file')
        chip_file = request.files.get('chip_file')
        tf_file = request.files.get('tf_file')
        pk_file = request.files.get('pk_file')


        # Save data to database
        data_prefix = str(int(time.time()*100))

        _save_file(expr_file, data_prefix, 'expr')
        _save_file(pk_file, data_prefix, 'pk')

        # check if file is too large, return
        try:
            df = pd.read_csv(join(current_app.config['UPLOAD_FOLDER'],
                            data_prefix + '_expr.csv'), sep='\t')
        except:
            return render_template('message_form.html', title='Invalid Expression file',
                                   message=['Expression File format is invalid, please check the sample file.'])

        num_genes =  df.shape[1]
        if num_genes > current_app.config['MAX_NUM_GENES']:
            log.error('User tried to upload lareg expression file with size: '.format(df.shape))
            msg = ['Number of genes in gene expression file is {}'.format(num_genes),
                   'The maximum number of genes allowed is {}'.format(current_app.config['MAX_NUM_GENES']),
                   'Please upload a smaller gene expression file.']
            return render_template('message_form.html', title='Expression file is too large', message=msg)


        if tf_file:
            _save_file(tf_file, data_prefix, 'tf')
            err = validator.validate_expr_tf_files(
                join(current_app.config['UPLOAD_FOLDER'], data_prefix + '_expr.csv'),
                join(current_app.config['UPLOAD_FOLDER'], data_prefix + '_tf.csv')
            )
            print(type(err))
            if len(err) != 0:
                log.error('Ivalid input from user for data_prefix: ' + data_prefix)
                log.info(err)
                return render_template('message_form.html', title='Invalid Input files', message=[err])
        else:  # no TF file given (Bad, so many TFs!)
            _generate_tf(data_prefix)

        if chip_file:
            _save_file(chip_file, data_prefix, 'chip')
        else:
            _generate_metadata(is_timeseries, data_prefix)

        job_id = db.add_network(dataset_name=dataset_name, email=email,
                                data_prefix=data_prefix, half_life=half_life)
        log.info('Database job created with id={}, data_prefix={}'
                 .format(job_id, data_prefix))

        submit_peak_job.delay(job_id=job_id, network_prefix=data_prefix,
                              data_path=current_app.config['UPLOAD_FOLDER'],
                              half_life=half_life)

        log.info('Job submitted to the queue.')

        msg = ['Job ID = '+ str(job_id),
               # "An email will be sent to you when the job is done.",
               "Your job is submitted to the queue. " +
                "You can access your results from: File -> Load job by ID."]

        return render_template('message_form.html', title='Job submitted', message=msg)

    # Return the user upload data form
    elif request.method == 'GET':
        formId = request.args.get('formId')
        return render_template('base_form.html',
                               formID = formId,
                               datasets = ds.DREAM5_datasets,
                               okBtn = 'Generate Regulatory Network') # load empty form

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _save_file(file, prefix, sufix):
    # upload_name = secure_filename(file.filename)
    # filename = str(prefix) + '_' + upload_name + sufix

    filename = join(current_app.config['UPLOAD_FOLDER'],
                            prefix + '_' + sufix + '.csv')

    if file:
        file.save(filename)
    else:
        open(filename, 'w').close() # create empty file

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_file_path(prefix, sufix):
    return join(current_app.config['UPLOAD_FOLDER'],
                            prefix + '_' + sufix + '.csv')

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _generate_tf(data_prefix):
    expr_path = join(current_app.config['UPLOAD_FOLDER'],
                             data_prefix + '_expr.csv')
    tf_path = join(current_app.config['UPLOAD_FOLDER'],
                             data_prefix + '_tf.csv')
    expr = pd.read_csv(expr_path, header=None, sep='\t')

    expr.iloc[0].to_csv(tf_path, index=False)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _generate_metadata(is_timeseries, data_prefix):
    expr_path = join(current_app.config['UPLOAD_FOLDER'],
                             data_prefix + '_expr.csv')


    chip_path = join(current_app.config['UPLOAD_FOLDER'],
                           data_prefix + '_chip.csv')
    expr = pd.read_csv(expr_path, sep='\t')


    chip = pd.DataFrame(columns=['#Experiment', 'Perturbations',
                'PerturbationLevels', 'Treatment', 'DeletedGenes',
                'OverexpressedGenes', 'Time', 'Repeat'])

    for i in range(expr.shape[0]):
        chip.loc[i, :] = 'NA'
        chip.loc[i, 'Repeat'] = 1
        chip.loc[i, '#Experiment'] = i + 1
        if (is_timeseries):
            pass            # Todo

    chip.to_csv(chip_path, index=False, sep='\t')

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@main.route("/add", methods=['GET', 'POST'])
def add_edge():
    str = 'hello there, add edge'
    return str

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@main.route('/user/<username>')
def user(username):
    return ""

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@main.route('/_add_numbers')
def add_numbers():
    a = request.args.get('a', 0, type=int)
    b = request.args.get('b', 0, type=int)
    return jsonify(result=a + b)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def mainTest():
    loadDemoGraphList()
    data = loadGraph()
    print(data)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# if __name__ == "__main__":
#     app.run(host='0.0.0.0',port=5500,debug=True)
#     # app.run(host='localhost', port=5000, debug=True)
#     # app.run()
#     # mainTest()
