# DeTangle 
A Web Application for gene regulatory network prediction

1- Setup the server:
-----------------

Create conda env and install requirements.

```bash
conda create -n detangle_env pip
conda activate detangle_env
pip install -r requirements.txt
```

Next, build and install *my fork of scikit-learn* in that environment.
Follow instruction in installation.txt for DB and R.

### Setup and install Postgres DB
Check online documentation

### Create Environemnet file

Create `.env` file on the repo root with the DB URI:

`vi .env`
then past:
```bash
DATABASE_URL='postgresql+psycopg2://detangle_user:somepass@localhost/detangle'
ENV_PATH='/home/soma/anaconda2/envs/detangle_env/lib/python2.7/site-packages'
```

### When using Apache:

Documentation on how to setup Flask app and Apache can be found online (instructions change over time)

Hint: Copy the DeTangle.conf into Apache and the starting point will be detangle.wsgi 
This file SHOULD have the correct conda env path:
  - detangle.wsgi
  


2- Start the Production Server (current):
------------------------------------------

This is running on the production server. You need to have three processes running, Redis (the message queue server), 
celery worker, and the flask app (as an apache service). If any of them hangs, the website won't work.

If the VM is restarted, the Flask app starts automatically, but both Redis and the celery worker need to be started manually *in order*.


1. Redis:

```
cd /var/www/DeTangle/
./run-redis.sh
```

2. Celery worker

Run this script and make sure the celery worker is running:

```bash
cd /var/www/DeTangle/
./run-celery-worker.sh
```
Note that the above scripts run the process in the background using the Ampersand (&) symbol (see the above files content). 
If this is not used, the two commands will need to run in different terminals.

3. Run/restart the Flask app (celery client/producer)

For Apache, restart the service IF you made any change to the Python code of DeTangel (exact comand depends on Linux distribution)

`sudo systemctl restart httpd.service`

If in local machine without apache use:
`flask run`

3- If the server has issues and not accepting submissions:
-----------------------------------------------------


### Find the current running process and kill it, then re-run the server using the steps above:

The following are useful commands to use, no need to run them all, the goal is to kill the hanging process (usually celery).

```bash

## ---- Check running processes tree:
ps -e -o pid,args --forest

# top memory usage:
ps -eo start,pid,ppid,cmd,%mem,%cpu --sort=-%mem | head

# check memory
free -mh

##---- kill celery:

# find the parent process (cut the o/p for visibility)
ps -eo pid,ppid,cmd  | grep celery | cut -c1-140
# then
kill -9 pid1
kill -9 pid2

# OR
killall -9 celery

# find R processes
ps -eo pid,ppid,%mem,cmd  | grep /R/ | cut -c1-140

# in ubunu
killall -9 /usr/lib/R/bin/exec/R
# or in Centos (current production server)
killall -9 /usr/lib64/R/bin/exec/R


```

### To check the logs:

```bash
## 1- PEAK logging:
tail -n 100 /var/www/DeTangle/log/run_PEAK_err.log  

# (print, including raise errors)
tail -n 100 /var/www/DeTangle/log/run_PEAK_out.log  

## 2- Flask app logging:
tail /www/var/DeTangle/log/app.log
```