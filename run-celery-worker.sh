#!/usr/bin/env bash

source activate detangle_env
celery worker --concurrency 1 -A celery_worker.celery --loglevel=info &