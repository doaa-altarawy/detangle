#!/usr/bin/env python

import os
from DeTangle import celery, create_app
import logging.config, yaml
from dotenv import load_dotenv


dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

dict_conf = yaml.load(open(os.path.join(os.path.dirname(__file__), 'logging.yml')),
            Loader=yaml.FullLoader)
dict_conf['handlers']['file']['filename'] = 'log/run_peak_task.log'
logging.config.dictConfig(dict_conf)


app = create_app(os.getenv('FLASK_CONFIG') or 'default')
app.app_context().push()
