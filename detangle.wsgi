import site
import sys
import logging
import os


""" Work around to enabe the virtual env"""

# 1- Remember original sys.path.
prev_sys_path = list(sys.path)

# 2- Add site-packages directory.
env_path = os.environ.get('ENV_PATH')
site.addsitedir(env_path)

# 3- Reorder sys.path so new directories at the front.
new_sys_path = []
for item in list(sys.path):
    if item not in prev_sys_path:
        new_sys_path.append(item)
        sys.path.remove(item)
        sys.path[:0] = new_sys_path
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


sys.path.insert(0, '/var/www/DeTangle')
#logging.basicConfig(stream=sys.stderr)
logging.basicConfig(filename='/var/www/DeTangle/log/app.log', level=logging.DEBUG,
        format='%(levelname)s: %(asctime)s: %(filename)s-%(funcName)s-%(lineno)d: %(message)s', datefmt='%m/%d/%Y-%H:%M')

logging.info(sys.path)
logging.info(sys.version_info)

# Putting the import at the end avoids the circular import error.

from detangle_web import app as application
application.secret_key = 'gfjkfjhffy'
